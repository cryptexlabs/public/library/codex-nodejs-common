import { HttpStatus } from "@nestjs/common";
import { LocaleInterface, MessageMetaHttpInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";
export declare class HttpResponseMeta extends MessageMeta implements MessageMetaHttpInterface {
    readonly status: HttpStatus;
    readonly path: string;
    constructor(status: HttpStatus, type: string, locale: LocaleInterface, config: DefaultConfig, correlationId: string, started: Date, path: string);
    toJSON(): any;
}
