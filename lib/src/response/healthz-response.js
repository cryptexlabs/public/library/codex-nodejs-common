"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HealthzResponse = void 0;
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const uuid_1 = require("uuid");
const locales_1 = require("../locales/locales");
const enum_1 = require("../locales/enum");
const http_response_meta_1 = require("./http-response-meta");
class HealthzResponse {
    constructor(status, locale, config) {
        this.locale = locale;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, config.getMetaType(codex_data_model_1.CodexMetaTypeEnum.NA_HTTP_HEALTHZ), locale, config, (0, uuid_1.v4)(), undefined, undefined);
        if (status >= 200 && status < 400) {
            this._phraseKey = enum_1.LocalesEnum.I_AM_HEALTHY;
        }
        else if (status >= 400 && status < 500) {
            this._phraseKey = enum_1.LocalesEnum.INVALID_REQUEST;
        }
        else {
            this._phraseKey = enum_1.LocalesEnum.UNKNOWN_ERROR;
        }
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
    get data() {
        return locales_1.i18nData.__({ phrase: this._phraseKey, locale: this.locale.i18n });
    }
}
exports.HealthzResponse = HealthzResponse;
//# sourceMappingURL=healthz-response.js.map