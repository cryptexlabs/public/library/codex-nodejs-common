import { HttpStatus } from "@nestjs/common";
import { MessageInterface, MessageMetaHttpInterface } from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
export declare class RestResponse<T> implements JsonSerializableInterface<MessageInterface<T>> {
    readonly data: T;
    readonly path?: string;
    readonly meta: MessageMetaHttpInterface;
    constructor(context: Context, status: HttpStatus, type: string, data: T, path?: string);
    toJSON(): MessageInterface<T>;
}
