"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestPaginatedResponse = void 0;
const http_paginated_response_meta_1 = require("./http-paginated-response-meta");
class RestPaginatedResponse {
    constructor(context, status, totalRecords, type, data) {
        this.data = data;
        this.meta = new http_paginated_response_meta_1.HttpPaginatedResponseMeta(status, totalRecords, type, context.locale, context.config, context.correlationId, context.started, context.path);
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
}
exports.RestPaginatedResponse = RestPaginatedResponse;
//# sourceMappingURL=rest.paginated.response.js.map