"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextualHttpResponse = void 0;
const http_response_1 = require("./http-response");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
class ContextualHttpResponse extends http_response_1.HttpResponse {
    constructor(context, status, i18nInstance, phraseKey) {
        super(status, context.config.getMetaType(codex_data_model_1.CodexMetaTypeEnum.NA_HTTP_SIMPLE), context.locale, context.config, context.correlationId, context.started, context.path);
        this.meta.context = context.messageContext
            ? context.messageContext
            : this.meta.context;
        this._message = i18nInstance.__({
            phrase: phraseKey,
            locale: context.locale.i18n,
        });
    }
    get data() {
        return this._message;
    }
}
exports.ContextualHttpResponse = ContextualHttpResponse;
//# sourceMappingURL=contextual-http.response.js.map