"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestResponse = void 0;
const http_response_meta_1 = require("./http-response-meta");
class RestResponse {
    constructor(context, status, type, data, path) {
        this.data = data;
        this.path = path;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, type, context.locale, context.config, context.correlationId, context.started, context.path);
        this.meta.context = context.messageContext;
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
}
exports.RestResponse = RestResponse;
//# sourceMappingURL=rest.response.js.map