import { HttpResponse } from "./http-response";
import { HttpStatus } from "@nestjs/common";
import { JsonSerializableInterface } from "../message";
import { MessageInterface } from "@cryptexlabs/codex-data-model";
import { Context } from "../context";
import { I18n } from "i18n";
export declare class ContextualHttpResponse<T> extends HttpResponse<T> implements JsonSerializableInterface<MessageInterface<T>> {
    private readonly _message;
    constructor(context: Context, status: HttpStatus, i18nInstance: I18n, phraseKey: string);
    get data(): string;
}
