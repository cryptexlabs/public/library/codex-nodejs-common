import { HttpStatus } from "@nestjs/common";
import { MessageInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
export declare class RestPaginatedResponse<L> implements JsonSerializableInterface<MessageInterface<L[]>> {
    readonly data: L[];
    readonly meta: MessageMetaInterface;
    constructor(context: Context, status: HttpStatus, totalRecords: number, type: string, data: L[]);
    toJSON(): MessageInterface<L[]>;
}
