"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleHttpResponse = void 0;
const contextual_http_response_1 = require("./contextual-http.response");
class SimpleHttpResponse extends contextual_http_response_1.ContextualHttpResponse {
    constructor(context, httpStatus, phraseKey) {
        super(context, httpStatus, context.i18n, phraseKey);
    }
}
exports.SimpleHttpResponse = SimpleHttpResponse;
//# sourceMappingURL=simple-http.response.js.map