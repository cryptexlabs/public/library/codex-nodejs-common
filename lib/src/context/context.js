"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Context = void 0;
const logger_1 = require("../logger");
const message_1 = require("../message");
class Context {
    constructor(correlationId, logger, config, client, locale, messageContext, started, i18n, path) {
        this.correlationId = correlationId;
        this.config = config;
        this.client = client;
        this.locale = locale;
        this.messageContext = messageContext;
        this.i18n = i18n;
        this.path = path;
        this.logger = new logger_1.ContextLogger(correlationId, config, client, logger);
        this.started = started || new Date(Date.now());
    }
    now() {
        if (this._now) {
            return this._now;
        }
        return new Date();
    }
    setNow(now) {
        this._now = now;
    }
    getMessageMeta(type) {
        return new message_1.MessageMeta(type, this.locale, this.config, this.correlationId, this.started);
    }
}
exports.Context = Context;
//# sourceMappingURL=context.js.map