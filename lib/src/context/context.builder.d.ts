import { LoggerService } from "@nestjs/common";
import { ApiMetaHeadersInterface, ClientInterface, LocaleInterface, MessageContextInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { Context } from "./context";
export declare class ContextBuilder {
    private readonly logger;
    private readonly config;
    client: ClientInterface;
    private readonly messageContext;
    private i18nData?;
    private locale;
    private correlationId;
    private started;
    private _path?;
    private _now?;
    constructor(logger: LoggerService, config: DefaultConfig, client: ClientInterface, messageContext: MessageContextInterface, i18nData?: i18nAPI);
    now(): Date;
    setNow(now: Date): void;
    build(): ContextBuilder;
    setI18nData(i18nData: i18nAPI): this;
    setMetaFromHeaders(headers: ApiMetaHeadersInterface & {
        "x-forwarded-uri"?: string;
    }): this;
    setMetaFromHeadersForNewMessage(headers: ApiMetaHeadersInterface & {
        "x-forwarded-uri"?: string;
    }): this;
    setMeta(meta: MessageMetaInterface): this;
    setCorrelationId(correlationId: string): ContextBuilder;
    setLocale(locale: LocaleInterface): this;
    setStarted(date: Date | string): this;
    private _getStarted;
    private _getCorrelationId;
    private _getLocale;
    getResult(): Context;
}
