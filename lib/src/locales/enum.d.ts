export declare enum LocalesEnum {
    UNKNOWN_ERROR = "unknown-error",
    I_AM_HEALTHY = "i-am-healthy",
    INVALID_REQUEST = "invalid-request",
    SUCCESS = "success",
    PONG = "pong",
    AN_ERROR_OCCURRED = "an-error-occurred",
    LANGUAGE_NOT_SUPPORTED = "language-not-supported"
}
