"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.i18n = exports.i18nData = void 0;
const i18n_1 = require("i18n");
const packageJSON = require("../../package.json");
const index_1 = require("./index");
const i18nData = {};
exports.i18nData = i18nData;
const i18nInstance = new i18n_1.I18n();
exports.i18n = i18nInstance;
i18nInstance.configure({
    locales: packageJSON.i18n.languages,
    defaultLocale: "en-US",
    register: i18nData,
    updateFiles: false,
    objectNotation: true,
    autoReload: false,
    syncFiles: false,
    extension: ".json",
    staticCatalog: {
        "en-US": index_1.en_US,
    },
});
//# sourceMappingURL=locales.js.map