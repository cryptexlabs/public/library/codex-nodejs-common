"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Client = void 0;
class Client {
    constructor(id, version, name, variant) {
        this.id = id;
        this.version = version;
        this.name = name;
        this.variant = variant;
    }
}
exports.Client = Client;
//# sourceMappingURL=client.js.map