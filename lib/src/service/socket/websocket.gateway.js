"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebsocketGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const common_1 = require("@nestjs/common");
const config_1 = require("../../config");
const socket_subscribed_client_1 = require("./socket-subscribed-client");
const broadcaster_service_1 = require("./broadcaster.service");
const consumer_1 = require("../consumer");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const response_1 = require("../../response");
const context_1 = require("../../context");
const locales_1 = require("../../locales/locales");
const enum_1 = require("../../locales/enum");
let WebsocketGateway = class WebsocketGateway {
    constructor(consumerService, config, authorizors, logger, broadcasterService, contextBuilder, authenticator) {
        this.consumerService = consumerService;
        this.config = config;
        this.authorizors = authorizors;
        this.logger = logger;
        this.broadcasterService = broadcasterService;
        this.contextBuilder = contextBuilder;
        this.authenticator = authenticator;
        this.subscribedTopics = [];
    }
    async afterInit() {
        await this.consumerService.init();
    }
    async handleConnection(client) {
        const token = client.protocol;
        if (!(await this.authenticator.authenticate(token))) {
            this.logger.error(`Unauthorized ${common_1.HttpStatus.UNAUTHORIZED}`);
            client.close(108, `Unauthorized ${common_1.HttpStatus.UNAUTHORIZED}`);
        }
        const subscribedClient = new socket_subscribed_client_1.SocketSubscribedClient(token, client, this.authorizors);
        this.logger.debug(`Client connected`);
        this.broadcasterService.subscribedClients.push(subscribedClient);
        client.onmessage = (event) => {
            this._onMessage(event, client, subscribedClient);
        };
    }
    async _onMessage(event, client, subscribedClient) {
        var _a, _b;
        let message;
        const now = new Date();
        try {
            this.logger.debug(`Received message: ${event.data}`);
            let context;
            if (!event.data) {
                context = this.contextBuilder.build().getResult();
                const userErrorMessage = locales_1.i18nData.__({
                    phrase: "an-error-occurred",
                    locale: context.locale.i18n,
                });
                const developerMessage = `Message body is empty`;
                this.logger.error(developerMessage);
                const errorMessage = new response_1.ErrorHttpResponse(common_1.HttpStatus.BAD_REQUEST, context.locale, { user: userErrorMessage, developer: developerMessage }, new Error().stack, this.config, message.meta.correlationId, now);
                client.send(JSON.stringify(errorMessage));
                return;
            }
            message = JSON.parse(event.data);
            context = this.contextBuilder.build().setMeta(message.meta).getResult();
            if (((_a = message.meta) === null || _a === void 0 ? void 0 : _a.type) ===
                this.config.getMetaType(codex_data_model_1.CodexMetaTypeEnum.NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED)) {
                await subscribedClient.subscribeToTopics(message.data.subscriptions);
                await this._subscribeTopics(this.broadcasterService.getAllTopicsForClients());
                const successMessage = new response_1.SimpleHttpResponse(context, common_1.HttpStatus.ACCEPTED, enum_1.LocalesEnum.SUCCESS);
                client.send(JSON.stringify(successMessage));
            }
            else if (((_b = message.meta) === null || _b === void 0 ? void 0 : _b.type) ===
                this.config.getMetaType(codex_data_model_1.CodexMetaTypeEnum.NA_WEBSOCKET_PING)) {
                context.logger.verbose(`Received ping from websocket client`);
            }
            else {
                const userErrorMessage = locales_1.i18nData.__({
                    phrase: "an-error-occurred",
                    locale: context.locale.i18n,
                });
                const developerMessage = `Unknown message type ${message.meta.type}`;
                this.logger.error(developerMessage);
                const errorMessage = new response_1.ErrorHttpResponse(common_1.HttpStatus.BAD_REQUEST, context.locale, { user: userErrorMessage, developer: developerMessage }, new Error().stack, this.config, message.meta.correlationId, now);
                client.send(JSON.stringify(errorMessage));
            }
        }
        catch (e) {
            const locale = new codex_data_model_1.Locale("en", "US");
            const userMessage = locales_1.i18nData.__({
                phrase: "unknown-error",
                locale: locale.i18n,
            });
            const developerErrorMessage = e.message || "Unknown Websocket Error";
            this.logger.error(developerErrorMessage, e.trace);
            const errorMessage = new response_1.ErrorHttpResponse(common_1.HttpStatus.BAD_REQUEST, locale, { user: userMessage, developer: developerErrorMessage }, new Error().stack, this.config, message.meta.correlationId, now);
            client.send(JSON.stringify(errorMessage));
        }
    }
    async _subscribeTopics(topics) {
        let didChangeSubscriptions = false;
        for (const subscription of topics) {
            if (!this.subscribedTopics.includes(subscription)) {
                didChangeSubscriptions = true;
                this.logger.verbose(`Subscribing to new topic: ${subscription}`);
                this.subscribedTopics.push(subscription);
            }
        }
        if (didChangeSubscriptions) {
            this.logger.verbose(`Subscriptions changed`, {
                subscribedTopics: this.subscribedTopics,
            });
            await this.consumerService.restart(this.subscribedTopics);
            this.logger.verbose(`Consumer restarted`);
        }
        else {
            this.logger.verbose(`Subscriptions did not change`);
        }
    }
    handleDisconnect(client) {
        for (let i = 0; i < this.broadcasterService.subscribedClients.length; i++) {
            if (this.broadcasterService.subscribedClients[i] === client) {
                this.broadcasterService.subscribedClients.splice(i, 1);
                break;
            }
        }
        this.broadcasterService.broadcast("disconnect", {});
    }
};
exports.WebsocketGateway = WebsocketGateway;
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", Object)
], WebsocketGateway.prototype, "server", void 0);
exports.WebsocketGateway = WebsocketGateway = __decorate([
    (0, websockets_1.WebSocketGateway)(),
    __param(0, (0, common_1.Inject)("WEBSOCKET_CONSUMER_SERVICE")),
    __param(1, (0, common_1.Inject)("CONFIG")),
    __param(2, (0, common_1.Inject)("TOPIC_AUTHORIZORS")),
    __param(3, (0, common_1.Inject)("LOGGER")),
    __param(4, (0, common_1.Inject)("BROADCASTER")),
    __param(5, (0, common_1.Inject)("CONTEXT_BUILDER")),
    __param(6, (0, common_1.Inject)("AUTHENTICATOR")),
    __metadata("design:paramtypes", [consumer_1.WebsocketConsumerService,
        config_1.DefaultConfig, Array, Object, broadcaster_service_1.BroadcasterService,
        context_1.ContextBuilder, Object])
], WebsocketGateway);
//# sourceMappingURL=websocket.gateway.js.map