"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsumerService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../config");
const event_1 = require("../../event");
const jsYaml = require("js-yaml");
const fs = require("fs");
const util_1 = require("../../util");
const consumer_util_1 = require("./consumer.util");
const healthz_1 = require("../healthz");
const randomstring = require("randomstring");
let ConsumerService = class ConsumerService {
    constructor(healthzService, consumerDelegateService, config, logger, externalFactory, internalFactory, consumerType) {
        this.healthzService = healthzService;
        this.consumerDelegateService = consumerDelegateService;
        this.config = config;
        this.logger = logger;
        this.externalFactory = externalFactory;
        this.internalFactory = internalFactory;
        this.consumerType = consumerType;
        this.topics = jsYaml.safeLoad(fs.readFileSync(config.kafka.topicsConfigPath).toString());
        this.eventHandler = new event_1.EventHandler(logger);
        logger.debug(`Initializing consumer with type: ${consumerType}`);
    }
    async start(consumerConfig = {}) {
        await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        if (this.config.kafka.defaultKafkaErrorTopic) {
            await this.consumerDelegateService.initializeProducer();
        }
        await this.consumerDelegateService.initializeConsumer(`${this.config.appName}-${this.consumerType}`, consumerConfig);
        await this.consumerDelegateService.connect();
        const topics = await this._getTopics();
        await this.consumerDelegateService.startConsumer(topics, async (topic, message) => {
            await this.handle(topic, message);
        }, async (error) => {
            this.logger.error(error);
            await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        });
        await this.healthzService.makeHealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
    }
    _getInternalTopics() {
        const internalTopicTypeMap = {
            [config_1.ConsumerTypeEnum.DEFAULT]: "internal",
            [config_1.ConsumerTypeEnum.GPU]: "gpu",
        };
        return this.topics.topics[internalTopicTypeMap[this.consumerType]];
    }
    async _getTopics() {
        let allTopics = [];
        if (this.consumerType === config_1.ConsumerTypeEnum.DEFAULT) {
            allTopics = allTopics.concat(this.topics.topics.external);
            allTopics = allTopics.concat(this.topics.topics.internal);
            this.logger.debug(`Subscribing to external topics: ${JSON.stringify(this.topics.topics.external)}`);
            this.logger.debug(`Subscribing to internal topics: ${JSON.stringify(this.topics.topics.internal)}`);
        }
        if (this.consumerType === config_1.ConsumerTypeEnum.GPU) {
            allTopics = allTopics.concat(this.topics.topics.gpu);
            this.logger.debug(`Subscribing to gpu topics: ${JSON.stringify(this.topics.topics.gpu)}`);
        }
        return consumer_util_1.ConsumerUtil.getTopics(allTopics);
    }
    async _errorEventHandler(message, errorMessage) {
        const messageWithError = Object.assign({}, message);
        messageWithError.meta.errorMessage = errorMessage;
        await this.consumerDelegateService.publish(this.config.kafka.defaultKafkaErrorTopic, Object.assign({}, messageWithError));
    }
    async handle(topic, message) {
        var _a, _b;
        const data = JSON.parse(message.value.toString());
        const messageTopic = (_b = (_a = data.meta) === null || _a === void 0 ? void 0 : _a.type) !== null && _b !== void 0 ? _b : topic;
        const handler = this._getFactory(messageTopic).getHandler(messageTopic);
        let errorEventHandler;
        if (this.config.kafka.defaultKafkaErrorTopic)
            errorEventHandler = this._errorEventHandler.bind(this);
        await this.eventHandler.handle(message, messageTopic, handler, errorEventHandler);
    }
    _getFactory(topic) {
        for (const testTopic of this.topics.topics.external) {
            if (util_1.StringUtil.stringMatches(testTopic, topic)) {
                return this.externalFactory;
            }
        }
        for (const testTopic of this._getInternalTopics()) {
            if (util_1.StringUtil.stringMatches(testTopic, topic)) {
                return this.internalFactory;
            }
        }
        throw new Error(`Could not match any factory for topic: ${topic}`);
    }
};
exports.ConsumerService = ConsumerService;
exports.ConsumerService = ConsumerService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("HEALTHZ_SERVICE")),
    __param(1, (0, common_1.Inject)("CONSUMER_SERVICE_DELEGATE")),
    __param(2, (0, common_1.Inject)("CONFIG")),
    __param(3, (0, common_1.Inject)("LOGGER")),
    __param(4, (0, common_1.Inject)("EXTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")),
    __param(5, (0, common_1.Inject)("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")),
    __param(6, (0, common_1.Inject)("CONSUMER_TYPE")),
    __metadata("design:paramtypes", [healthz_1.HealthzService, Object, config_1.DefaultConfig, Object, Object, Object, String])
], ConsumerService);
//# sourceMappingURL=consumer.service.js.map