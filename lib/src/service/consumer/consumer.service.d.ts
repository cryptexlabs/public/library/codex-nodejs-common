import { LoggerService } from "@nestjs/common";
import { ConsumerServiceDelegateInterface } from "./consumer-service-delegate.interface";
import { DefaultConfig } from "../../config";
import { EventHandlerFactoryInterface } from "../../event";
import { HealthzService } from "../healthz";
export declare class ConsumerService {
    private readonly healthzService;
    private readonly consumerDelegateService;
    private readonly config;
    private readonly logger;
    private readonly externalFactory;
    private readonly internalFactory;
    private readonly consumerType;
    private topics;
    private eventHandler;
    constructor(healthzService: HealthzService, consumerDelegateService: ConsumerServiceDelegateInterface, config: DefaultConfig, logger: LoggerService, externalFactory: EventHandlerFactoryInterface, internalFactory: EventHandlerFactoryInterface, consumerType: string);
    start(consumerConfig?: {}): Promise<void>;
    private _getInternalTopics;
    private _getTopics;
    private _errorEventHandler;
    handle(topic: string, message: any): Promise<void>;
    _getFactory(topic: string): EventHandlerFactoryInterface;
}
