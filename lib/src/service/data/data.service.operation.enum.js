"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataServiceOperationEnum = void 0;
var DataServiceOperationEnum;
(function (DataServiceOperationEnum) {
    DataServiceOperationEnum["CREATED"] = "created";
    DataServiceOperationEnum["UPDATED"] = "updated";
    DataServiceOperationEnum["NOOP"] = "noop";
})(DataServiceOperationEnum || (exports.DataServiceOperationEnum = DataServiceOperationEnum = {}));
//# sourceMappingURL=data.service.operation.enum.js.map