export declare enum DataServiceOperationEnum {
    CREATED = "created",
    UPDATED = "updated",
    NOOP = "noop"
}
