import { LoggerService } from "@nestjs/common";
export declare class KafkaLogger {
    private readonly logger;
    constructor(logger: LoggerService);
    private toContext;
    log({ level, log }: {
        level: any;
        log: any;
    }): void;
}
