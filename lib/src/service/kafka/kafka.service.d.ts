import { LoggerService } from "@nestjs/common";
import { ConsumerConfig, Message } from "kafkajs";
import { DefaultConfig } from "../../config";
import { ConsumerServiceDelegateInterface } from "../consumer";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@cryptexlabs/codex-data-model";
export declare class KafkaService implements KafkaServiceInterface, ConsumerServiceDelegateInterface {
    private readonly config;
    private readonly logger;
    private _kafka;
    private _kafkaConsumer;
    private _kafkaProducer;
    private _kafkaAdmin;
    constructor(config: DefaultConfig, logger: LoggerService, extraConfig?: any);
    stopConsumer(): Promise<void>;
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    initializeConsumer(consumerGroup: string, config?: ConsumerConfig): Promise<void>;
    initializeProducer(): Promise<void>;
    startConsumer(topics: (string | RegExp)[], callback: (topic: string, message: Message) => Promise<void>, err?: (error: string) => Promise<void>): Promise<void>;
    startManualConsumer(topics: (string | RegExp)[], callback: (topic: string, message: Message) => Promise<void>): Promise<void>;
    ensureTopicsExist(topics: (string | RegExp)[], waitForLeaders?: boolean): Promise<void>;
    publish(topic: string, payload: MessageInterface<any>): Promise<void>;
    publishBulk(topic: string, payloads: any[]): Promise<void>;
    private _setManualConsume;
}
