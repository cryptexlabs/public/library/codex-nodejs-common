import { LoggerService } from "@nestjs/common";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@cryptexlabs/codex-data-model";
import { Message } from "kafkajs";
export declare class KafkaStubService implements KafkaServiceInterface {
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    initializeConsumer(consumerGroup: string, config?: {}): Promise<void>;
    initializeProducer(): Promise<void>;
    publish(topic: string, payload: any): Promise<void>;
    publishBulk(topic: string, payloads: any[]): Promise<void>;
    send(payload: MessageInterface<any>, logger: LoggerService): Promise<any>;
    startConsumer(topics: (string | RegExp)[], callback: (topic: string, message: Message) => Promise<void>, err?: (error: string) => Promise<void>): Promise<void>;
    startManualConsumer(topics: (string | RegExp)[], callback: (topic: string, message: Message) => Promise<void>): Promise<void>;
    startProducer(): Promise<any>;
    stopConsumer(): Promise<void>;
    ensureTopicsExist(topics: (string | RegExp)[], waitForLeaders?: boolean): Promise<void>;
}
