"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaService = void 0;
const common_1 = require("@nestjs/common");
const kafkajs_1 = require("kafkajs");
const config_1 = require("../../config");
const kafka_logger_1 = require("./kafka.logger");
let KafkaService = class KafkaService {
    constructor(config, logger, extraConfig = {}) {
        this.config = config;
        this.logger = logger;
        const kafkaClientConfig = Object.assign({ clientId: config.clientId, brokers: config.kafka.endpoint.split(","), sasl: undefined, ssl: undefined }, extraConfig);
        if (config.kafka.username) {
            kafkaClientConfig.sasl = {
                mechanism: config.kafka.saslMechanism.toLowerCase(),
                username: config.kafka.username,
                password: config.kafka.password,
            };
            kafkaClientConfig.ssl = true;
        }
        const kafkaLogger = new kafka_logger_1.KafkaLogger(logger);
        this._kafka = new kafkajs_1.Kafka(Object.assign(Object.assign({}, kafkaClientConfig), { logCreator: (logLevel) => {
                return kafkaLogger.log.bind(kafkaLogger);
            } }));
    }
    async stopConsumer() {
        await this._kafkaConsumer.stop();
    }
    async connect() {
        if (this._kafkaConsumer) {
            await this._kafkaConsumer.connect();
        }
        if (this._kafkaProducer) {
            await this._kafkaProducer.connect();
        }
        this._kafkaAdmin = this._kafka.admin();
        await this._kafkaAdmin.connect();
    }
    async disconnect() {
        if (this._kafkaConsumer) {
            await this._kafkaConsumer.disconnect();
        }
        if (this._kafkaProducer) {
            await this._kafkaProducer.disconnect();
        }
    }
    async initializeConsumer(consumerGroup, config = {}) {
        this._kafkaConsumer = this._kafka.consumer(Object.assign({ groupId: consumerGroup }, config));
    }
    async initializeProducer() {
        this._kafkaProducer = this._kafka.producer();
    }
    async startConsumer(topics, callback, err) {
        const promises = [];
        for (const topic of topics) {
            promises.push(this._kafkaConsumer.subscribe({ topic, fromBeginning: false }));
        }
        try {
            await Promise.all(promises);
        }
        catch (e) {
            this.logger.error(e.message, e.trace);
        }
        await this.ensureTopicsExist(topics);
        if (this.config.kafka.defaultKafkaErrorTopic) {
            await this.ensureTopicsExist([this.config.kafka.defaultKafkaErrorTopic]);
        }
        await this._kafkaConsumer.run({
            autoCommit: true,
            eachMessage: async ({ topic, partition, message }) => {
                this.logger.log({
                    topic,
                    partition,
                    message: {
                        key: message.key,
                        value: message.value.toString(),
                    },
                });
                await callback(topic, message);
            },
        });
        this._kafkaConsumer.on(this._kafkaConsumer.events.CRASH, async (event) => {
            var _a;
            const error = (_a = event === null || event === void 0 ? void 0 : event.payload) === null || _a === void 0 ? void 0 : _a.error;
            this.logger.log("Consumer is crashing!");
            if (error) {
                this.logger.error(JSON.stringify(error));
            }
            await err("Consumer crashed");
        });
    }
    async startManualConsumer(topics, callback) {
        await this._setManualConsume();
        const promises = [];
        for (const topic of topics) {
            promises.push(this._kafkaConsumer.subscribe({ topic, fromBeginning: true }));
        }
        try {
            await Promise.all(promises);
            await this.ensureTopicsExist(topics);
            const results = [
                this._kafkaConsumer.run({
                    eachMessage: async (messagePayload) => {
                        const { topic, partition, message } = messagePayload;
                        await callback(topic, message);
                    },
                }),
            ];
            await Promise.all(results);
        }
        catch (e) {
            this.logger.error(e.message, e.trace);
        }
    }
    async ensureTopicsExist(topics, waitForLeaders = true) {
        const stringTopics = [];
        for (const topic of topics) {
            if (typeof topic === "string") {
                stringTopics.push(topic);
            }
        }
        const currentTopics = await this._kafkaAdmin.listTopics();
        const createTopics = [];
        for (const topic of stringTopics) {
            if (!currentTopics.includes(topic)) {
                const createTopic = {
                    numPartitions: this.config.kafka.defaultPartitions,
                    topic,
                };
                createTopics.push(createTopic);
            }
        }
        if (createTopics.length > 0) {
            await this._kafkaAdmin.createTopics({
                waitForLeaders,
                topics: createTopics,
            });
        }
    }
    async publish(topic, payload) {
        const json = JSON.stringify(payload);
        this.logger.debug(`Publishing kafka message: ${json} to topic ${topic}`);
        await this._kafkaProducer.send({
            topic,
            messages: [{ value: json }],
        });
    }
    async publishBulk(topic, payloads) {
        const messages = [];
        for (const payload of payloads) {
            messages.push({ value: JSON.stringify(payload), key: topic });
        }
        await this._kafkaProducer.send({
            topic,
            messages,
        });
    }
    async _setManualConsume() {
        let consumedTopicPartitions = {};
        this._kafkaConsumer.on(this._kafkaConsumer.events.GROUP_JOIN, async ({ payload }) => {
            const { memberAssignment } = payload;
            consumedTopicPartitions = Object.entries(memberAssignment).reduce((topics, [topic, partitions]) => {
                for (const partition in partitions) {
                    this.logger.debug("topic-partition: ", `${topic}-${partition}`);
                    topics[`${topic}-${partition}`] = false;
                }
                return topics;
            }, {});
        });
        let processedBatch = true;
        this._kafkaConsumer.on(this._kafkaConsumer.events.FETCH_START, async () => {
            if (processedBatch === false) {
                await this._kafkaConsumer.disconnect();
                process.exit(0);
            }
            processedBatch = false;
        });
        this._kafkaConsumer.on(this._kafkaConsumer.events.END_BATCH_PROCESS, async ({ payload }) => {
            const { topic, partition, offsetLag } = payload;
            consumedTopicPartitions[`${topic}-${partition}`] = offsetLag === "0";
            if (Object.values(consumedTopicPartitions).every((consumed) => Boolean(consumed))) {
                await this._kafkaConsumer.disconnect();
                process.exit(0);
            }
            processedBatch = true;
        });
        const errorTypes = ["unhandledRejection", "uncaughtException"];
        const signalTraps = ["SIGTERM", "SIGINT", "SIGUSR2"];
        errorTypes.map((type) => {
            process.on(type, async (e) => {
                try {
                    this.logger.debug(`process.on ${type}`);
                    this.logger.error(e);
                    await this.disconnect();
                    process.exit(0);
                }
                catch (_) {
                    process.exit(1);
                }
            });
        });
        signalTraps.map((type) => {
            process.once(type, async () => {
                try {
                    await this.disconnect();
                }
                finally {
                    process.kill(process.pid, type);
                }
            });
        });
    }
};
exports.KafkaService = KafkaService;
exports.KafkaService = KafkaService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("CONFIG")),
    __param(1, (0, common_1.Inject)("LOGGER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig, Object, Object])
], KafkaService);
//# sourceMappingURL=kafka.service.js.map