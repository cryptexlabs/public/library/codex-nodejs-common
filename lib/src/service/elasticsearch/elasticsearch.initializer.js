"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticsearchInitializer = void 0;
class ElasticsearchInitializer {
    constructor(logger, index, elasticsearchService, indexProperties, elasticsearchPinger, type) {
        this.logger = logger;
        this.index = index;
        this.elasticsearchService = elasticsearchService;
        this.indexProperties = indexProperties;
        this.elasticsearchPinger = elasticsearchPinger;
        this.type = type;
        this._indexInitialized = false;
    }
    async _createIndex() {
        await this.elasticsearchService.indices.create({
            index: this.index,
            body: {},
        });
    }
    async _getIndexExists() {
        return await this.elasticsearchService.indices.exists({
            index: this.index,
        });
    }
    async initializeIndex() {
        if (this._indexInitialized) {
            return;
        }
        this._indexInitialized = true;
        await this.elasticsearchPinger.waitForReady();
        const exists = await this._getIndexExists();
        if (!exists) {
            await this._createIndex();
        }
        let params = {
            index: this.index,
            body: {
                properties: this.indexProperties,
            },
        };
        if (this.type) {
            params = Object.assign(Object.assign({}, params), { type: this.type });
        }
        await this.elasticsearchService.indices.putMapping(params);
    }
}
exports.ElasticsearchInitializer = ElasticsearchInitializer;
//# sourceMappingURL=elasticsearch.initializer.js.map