import { CustomLogger } from "../../logger";
export declare class ElasticsearchPinger {
    private readonly elasticsearchService;
    private readonly logger;
    private intervalId;
    private ready;
    private isPinging;
    constructor(elasticsearchService: any, logger: CustomLogger);
    private _resetPinger;
    waitForReady(): Promise<unknown>;
    _isReady(): Promise<boolean>;
}
