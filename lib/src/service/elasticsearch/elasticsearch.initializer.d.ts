import { ElasticsearchService } from "@nestjs/elasticsearch";
import { CustomLogger } from "../../logger";
import { ElasticsearchPinger } from "./elasticsearch.pinger";
export declare class ElasticsearchInitializer {
    private readonly logger;
    private readonly index;
    private readonly elasticsearchService;
    private readonly indexProperties;
    private readonly elasticsearchPinger;
    private readonly type?;
    private _indexInitialized;
    constructor(logger: CustomLogger, index: string, elasticsearchService: ElasticsearchService, indexProperties: any, elasticsearchPinger: ElasticsearchPinger, type?: string);
    private _createIndex;
    private _getIndexExists;
    initializeIndex(): Promise<void>;
}
