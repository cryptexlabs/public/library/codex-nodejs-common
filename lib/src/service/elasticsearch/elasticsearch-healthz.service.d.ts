import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../../config";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { HealthzService } from "../healthz";
import { ElasticsearchPinger } from "./elasticsearch.pinger";
export declare class ElasticsearchHealthzService {
    private readonly config;
    private readonly logger;
    private readonly elasticsearchService;
    private readonly elasticsearchPinger;
    private readonly healthzService;
    private isWaitingForHealthy;
    private isStarted;
    constructor(config: DefaultConfig, logger: LoggerService, elasticsearchService: ElasticsearchService, elasticsearchPinger: ElasticsearchPinger, healthzService: HealthzService);
    start(): Promise<void>;
    waitForHealthy(): Promise<void>;
    private _ping;
}
