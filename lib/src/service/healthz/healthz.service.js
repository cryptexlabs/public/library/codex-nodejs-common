"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HealthzService = void 0;
const common_1 = require("@nestjs/common");
const fs = require("fs");
const config_1 = require("../../config");
let HealthzService = class HealthzService {
    constructor(config, logger) {
        this.config = config;
        this.logger = logger;
        this.componentHealth = {};
        this._isHealthy = false;
    }
    async isHealthy() {
        if (Object.keys(this.componentHealth).length === 0) {
            return true;
        }
        return this._allComponentsAreHealthy();
    }
    async makeHealthy(component) {
        this.componentHealth[component] = true;
        if (!this._allComponentsAreHealthy()) {
            return Promise.resolve();
        }
        return new Promise(async (resolve) => {
            if (fs.existsSync(this.config.healthzFilePath)) {
                resolve();
            }
            else {
                if (!this._isHealthy) {
                    this._isHealthy = true;
                    this.logger.debug("Making service healthy", "info");
                }
                fs.writeFile(this.config.healthzFilePath, "I'm healthy!", () => {
                    resolve();
                });
            }
        });
    }
    async makeUnhealthy(component) {
        this.componentHealth[component] = false;
        if (fs.existsSync(this.config.healthzFilePath)) {
            return new Promise((resolve) => {
                if (this._isHealthy) {
                    this._isHealthy = false;
                    this.logger.debug("Making service unhealthy");
                }
                fs.unlink(this.config.healthzFilePath, () => {
                    resolve();
                });
            });
        }
    }
    _allComponentsAreHealthy() {
        for (const componentName in this.componentHealth) {
            const healthy = this.componentHealth[componentName];
            if (!healthy) {
                this.logger.debug(`${componentName} is still unhealthy`);
                return false;
            }
        }
        return true;
    }
};
exports.HealthzService = HealthzService;
exports.HealthzService = HealthzService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("CONFIG")),
    __param(1, (0, common_1.Inject)("LOGGER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig, Object])
], HealthzService);
//# sourceMappingURL=healthz.service.js.map