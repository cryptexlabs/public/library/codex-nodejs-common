"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HealthzComponentEnum = void 0;
var HealthzComponentEnum;
(function (HealthzComponentEnum) {
    HealthzComponentEnum["DATABASE_REDIS"] = "database.redis";
    HealthzComponentEnum["DATABASE_ELASTICSEARCH"] = "database.elasticsearch";
    HealthzComponentEnum["DATABASE_MONGODB"] = "database.mongodb";
    HealthzComponentEnum["DATABASE_MYSQL"] = "database.mysql";
    HealthzComponentEnum["MESSAGE_BROKER_KAFKA"] = "message-broker.kafka";
})(HealthzComponentEnum || (exports.HealthzComponentEnum = HealthzComponentEnum = {}));
//# sourceMappingURL=healthz.component.enum.js.map