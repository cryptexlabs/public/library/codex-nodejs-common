import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../../config";
import { HealthzComponentEnum } from "./healthz.component.enum";
export declare class HealthzService {
    private readonly config;
    private readonly logger;
    private readonly componentHealth;
    private _isHealthy;
    constructor(config: DefaultConfig, logger: LoggerService);
    isHealthy(): Promise<boolean>;
    makeHealthy(component: HealthzComponentEnum | string): Promise<void>;
    makeUnhealthy(component: HealthzComponentEnum): Promise<void>;
    private _allComponentsAreHealthy;
}
