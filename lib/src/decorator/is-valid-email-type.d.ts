import { ValidatorConstraintInterface, ValidationArguments, ValidationOptions } from "class-validator";
export declare class IsValidEmailTypeConstraint implements ValidatorConstraintInterface {
    private readonly EMAIL_TYPES;
    validate(timezone: any, args: ValidationArguments): boolean;
    defaultMessage(args: ValidationArguments): string;
}
export declare function IsValidEmailType(validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
