"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsValidUtcTimezoneConstraint = void 0;
exports.IsValidUtcTimezone = IsValidUtcTimezone;
const class_validator_1 = require("class-validator");
let IsValidUtcTimezoneConstraint = class IsValidUtcTimezoneConstraint {
    constructor() {
        this.TIMEZONES = [
            "-12:00",
            "-11:00",
            "-10:30",
            "-10:00",
            "-09:30",
            "-09:00",
            "-08:00",
            "-07:00",
            "-06:00",
            "-05:00",
            "-04:00",
            "-04:30",
            "-03:30",
            "-03:00",
            "-02:00",
            "-01:00",
            "-00:00",
            "+00:00",
            "+01:00",
            "+02:00",
            "+03:00",
            "+03:30",
            "+04:00",
            "+04:30",
            "+05:00",
            "+05:30",
            "+05:45",
            "+06:00",
            "+06:30",
            "+07:00",
            "+08:00",
            "+08:45",
            "+09:00",
            "+09:30",
            "+10:00",
            "+10:30",
            "+11:00",
            "+12:00",
            "+12:45",
            "+13:00",
            "+14:00",
        ];
    }
    validate(timezone, args) {
        return this.TIMEZONES.includes(timezone);
    }
    defaultMessage(args) {
        return `"${args.value}" is not a valid UTC timezone. Choose one of the allowed timezones.`;
    }
};
exports.IsValidUtcTimezoneConstraint = IsValidUtcTimezoneConstraint;
exports.IsValidUtcTimezoneConstraint = IsValidUtcTimezoneConstraint = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: "IsValidUtcTimezone", async: false })
], IsValidUtcTimezoneConstraint);
function IsValidUtcTimezone(validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsValidUtcTimezoneConstraint,
        });
    };
}
//# sourceMappingURL=is-valid-utc-timezone.js.map