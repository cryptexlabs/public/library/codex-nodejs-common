import { ValidatorConstraintInterface, ValidationArguments, ValidationOptions } from "class-validator";
export declare class IsValidUtcTimezoneConstraint implements ValidatorConstraintInterface {
    private readonly TIMEZONES;
    validate(timezone: any, args: ValidationArguments): boolean;
    defaultMessage(args: ValidationArguments): string;
}
export declare function IsValidUtcTimezone(validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
