"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiPagination = ApiPagination;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
function ApiPagination() {
    return (0, common_1.applyDecorators)(...[
        (0, swagger_1.ApiQuery)({
            name: "page",
            required: true,
            example: 1,
        }),
        (0, swagger_1.ApiQuery)({
            name: "pageSize",
            required: true,
            example: 10,
        }),
    ]);
}
//# sourceMappingURL=api-pagination.js.map