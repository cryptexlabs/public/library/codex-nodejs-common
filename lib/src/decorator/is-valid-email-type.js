"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsValidEmailTypeConstraint = void 0;
exports.IsValidEmailType = IsValidEmailType;
const class_validator_1 = require("class-validator");
const authf_data_model_1 = require("@cryptexlabs/authf-data-model");
let IsValidEmailTypeConstraint = class IsValidEmailTypeConstraint {
    constructor() {
        this.EMAIL_TYPES = Object.values(authf_data_model_1.EmailTypeEnum);
    }
    validate(timezone, args) {
        return this.EMAIL_TYPES.includes(timezone);
    }
    defaultMessage(args) {
        return `"${args.value}" is not a valid UTC timezone. Choose one of the allowed timezones.`;
    }
};
exports.IsValidEmailTypeConstraint = IsValidEmailTypeConstraint;
exports.IsValidEmailTypeConstraint = IsValidEmailTypeConstraint = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: "IsValidEmailType", async: false })
], IsValidEmailTypeConstraint);
function IsValidEmailType(validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsValidEmailTypeConstraint,
        });
    };
}
//# sourceMappingURL=is-valid-email-type.js.map