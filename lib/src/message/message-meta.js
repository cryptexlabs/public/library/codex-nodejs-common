"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageMeta = void 0;
const uuid_1 = require("uuid");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const client_1 = require("../client");
class MessageMeta {
    get correlationId() {
        if (!this._correlationId) {
            this._correlationId = (0, uuid_1.v4)();
        }
        return this._correlationId;
    }
    constructor(type, locale, config, correlationId, started) {
        this.type = type;
        this.locale = locale;
        this.started = started;
        const now = new Date(Date.now());
        const start = started || now;
        this.time = {
            created: now.toISOString(),
            started: start.toISOString(),
        };
        this.context = {
            category: "default",
            id: "none",
        };
        this.interactionId = null;
        this.schemaVersion = codex_data_model_1.Version.CURRENT;
        this._correlationId = correlationId;
        this.client = new client_1.ServiceClient(config);
    }
    toJSON() {
        return {
            type: this.type,
            schemaVersion: this.schemaVersion,
            correlationId: this.correlationId,
            time: this.time,
            context: this.context,
            locale: this.locale,
            client: this.client,
        };
    }
}
exports.MessageMeta = MessageMeta;
//# sourceMappingURL=message-meta.js.map