import { EventHandlerInterface } from "./event.handler.interface";
import { EventHandlerFactoryInterface } from "./event.handler.factory.interface";
import { EventHandlerRegexInterface } from "./event.handler.regex.interface";
export declare abstract class EventHandlerFactory implements EventHandlerFactoryInterface {
    protected readonly handlerKeyValueMap: Record<string, EventHandlerInterface>;
    protected readonly handlerRegexList: EventHandlerRegexInterface[];
    protected constructor(handlerKeyValueMap: Record<string, EventHandlerInterface>, handlerRegexList: EventHandlerRegexInterface[]);
    getHandler(topic: string): EventHandlerInterface;
}
