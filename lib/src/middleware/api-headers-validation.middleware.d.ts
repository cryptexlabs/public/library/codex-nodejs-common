import { NestMiddleware } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ContextBuilder } from "../context";
export declare class ApiHeadersValidationMiddleware implements NestMiddleware {
    private readonly config;
    private readonly contextBuilder;
    constructor(config: DefaultConfig, contextBuilder: ContextBuilder);
    use(req: any, res: any, next: () => void): any;
}
