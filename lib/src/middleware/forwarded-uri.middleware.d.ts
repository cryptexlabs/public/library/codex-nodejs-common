import { NestMiddleware } from "@nestjs/common";
export declare class ForwardedUriMiddleware implements NestMiddleware {
    use(req: any, res: any, next: () => void): any;
}
