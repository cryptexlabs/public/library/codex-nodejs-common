"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiHeadersValidationMiddleware = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../config");
const util_1 = require("../util");
const context_1 = require("../context");
const date_util_1 = require("../util/date.util");
let ApiHeadersValidationMiddleware = class ApiHeadersValidationMiddleware {
    constructor(config, contextBuilder) {
        this.config = config;
        this.contextBuilder = contextBuilder;
    }
    use(req, res, next) {
        const requiredHeaders = {
            "x-correlation-id": (context, correlationId) => {
                if (correlationId.trim() === "") {
                    throw new common_1.BadRequestException(`X-Correlation-Id header is empty. Correlation Id is Unique identifier generated when a person takes an action or a system time based event is triggered. This correlation id should be passed between systems and should be only created if none exists. Stops being re-used when an action is required from a person. Format is UUID V4`);
                }
                if (!util_1.StringUtil.isValidUUIDv4(correlationId)) {
                    throw new common_1.BadRequestException(`Invalid X-Correlation-Id header. Must be UUID V4. For example: '57d7f725-420d-45b5-9f92-101c6c0cffc3'`);
                }
            },
            "accept-language": (context, acceptLanguage) => {
                if (acceptLanguage.trim() === "") {
                    throw new common_1.BadRequestException(`Accept-Language header is empty. For example 'en-US'`);
                }
                const parsedLanguage = util_1.LocaleUtil.getLocaleFromAcceptLanguageHeader(context, acceptLanguage);
                if (!parsedLanguage) {
                    throw new common_1.BadRequestException(`Invalid Accept-Language header. Must be ISO 639 alpha-1 language code. Or standard language supported by a web browser. For example: 'en-US' or 'en-US,en;q=0.9'`);
                }
            },
            "x-started": (context, started) => {
                if (started.trim() === "") {
                    throw new common_1.BadRequestException(`X-Started header is empty`);
                }
                if (!date_util_1.DateUtil.isValidISO8601Date(started)) {
                    throw new common_1.BadRequestException(`Invalid date format for X-Started header. Must be ISO 8601 format. For example '2024-02-29T19:47:57.366Z'`);
                }
            },
            "x-created": (context, created) => {
                if (created.trim() === "") {
                    throw new common_1.BadRequestException(`X-Created header is empty`);
                }
                if (!date_util_1.DateUtil.isValidISO8601Date(created)) {
                    throw new common_1.BadRequestException(`Invalid date format for X-Created header. Must be ISO 8601 format. For example '2024-02-29T19:47:57.366Z'`);
                }
            },
            "x-context-category": (context, contextCategory) => {
                if (contextCategory.trim() === "") {
                    throw new common_1.BadRequestException(`X-Context-Category header is empty. A category for the context of the request. For example 'test', 'performance test' or 'debug'`);
                }
            },
            "x-context-id": (context, contextId) => {
                if (contextId.trim() === "") {
                    throw new common_1.BadRequestException(`X-Context-Id header is empty`);
                }
                if (!util_1.StringUtil.isValidUUIDv4(contextId) && contextId !== "none") {
                    throw new common_1.BadRequestException(`Invalid X-Context-Id header. Must be UUID V4 or 'none'. A unique context identifier used for correlating logs and metrics usually for performance or experimental testing`);
                }
            },
            "x-client-id": (context, clientId) => {
                if (clientId.trim() === "") {
                    throw new common_1.BadRequestException(`X-Client-Id header is empty. A unique identifier for the client to help identify exactly which application or third party is making the requests. Should be identical for every client. Used to uniquely identify projects. You can hard code this string in your project. Its not a secret. Should NOT be different for every request.`);
                }
                if (!util_1.StringUtil.isValidUUIDv4(clientId)) {
                    throw new common_1.BadRequestException(`Invalid X-Client-Id header. Must be UUID V4 format`);
                }
            },
            "x-client-name": (context, clientName) => {
                if (clientName.trim() === "") {
                    throw new common_1.BadRequestException(`X-Client-Name header is empty. Canonical name of the client that is making the API call. Such as "BlueBerry Picker App"`);
                }
            },
            "x-client-version": (context, clientVersion) => {
                if (clientVersion.trim() === "") {
                    throw new common_1.BadRequestException(`X-Client-Version header is empty. Version of the client that is making the API call. Such as '1.3.8'`);
                }
            },
            "x-client-variant": (context, clientVariant) => {
                if (clientVariant.trim() === "") {
                    throw new common_1.BadRequestException("X-Client-Variant header is Empty. Variation of the client build. There can be different builds of the same version of a client such as 'dev', 'test' or 'demo'. Typically these variations would be configured to interact with different backends and have different debugging levels");
                }
            },
        };
        const optionalHeaders = {
            "x-now": (context, now) => {
                try {
                    if (now) {
                        new Date(now);
                    }
                }
                catch (e) {
                    throw new common_1.BadRequestException(`X-Now header is invalid: '${now}'`);
                }
            },
        };
        if (this.config.apiPrefixes.some((prefix) => {
            return req.path.startsWith(`/${prefix}`);
        })) {
            const missingHeaders = Object.keys(requiredHeaders).filter((header) => !req.headers[header.toLowerCase()]);
            if (missingHeaders.length > 0) {
                throw new common_1.BadRequestException(`Missing required headers: ${missingHeaders.join(", ")}`);
            }
            const validationContext = this.contextBuilder
                .build()
                .setMetaFromHeaders(req.headers)
                .getResult();
            for (const headerName of Object.keys(requiredHeaders)) {
                const validator = requiredHeaders[headerName];
                const header = req.headers[headerName];
                validator(validationContext, header);
            }
            for (const headerName of Object.keys(optionalHeaders)) {
                const validator = optionalHeaders[headerName];
                const header = req.headers[headerName];
                validator(validationContext, header);
            }
        }
        next();
    }
};
exports.ApiHeadersValidationMiddleware = ApiHeadersValidationMiddleware;
exports.ApiHeadersValidationMiddleware = ApiHeadersValidationMiddleware = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("CONFIG")),
    __param(1, (0, common_1.Inject)("CONTEXT_BUILDER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig,
        context_1.ContextBuilder])
], ApiHeadersValidationMiddleware);
//# sourceMappingURL=api-headers-validation.middleware.js.map