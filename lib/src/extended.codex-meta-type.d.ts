import { CodexMetaTypeEnum } from "@cryptexlabs/codex-data-model";
export declare const ExtendedCodexMetaType: {
    MY_TYPE: CodexMetaTypeEnum;
    NA_HTTP_ERROR: CodexMetaTypeEnum.NA_HTTP_ERROR;
    NA_HTTP_SIMPLE: CodexMetaTypeEnum.NA_HTTP_SIMPLE;
    NA_HTTP_HEALTHZ: CodexMetaTypeEnum.NA_HTTP_HEALTHZ;
    NA_WEBSOCKET_ERROR: CodexMetaTypeEnum.NA_WEBSOCKET_ERROR;
    NA_WEBSOCKET_SIMPLE: CodexMetaTypeEnum.NA_WEBSOCKET_SIMPLE;
    NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED: CodexMetaTypeEnum.NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED;
    NA_WEBSOCKET_PING: CodexMetaTypeEnum.NA_WEBSOCKET_PING;
};
