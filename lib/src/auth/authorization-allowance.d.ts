import { AuthorizationRequestInterface } from "./authorization-request.interface";
export declare class AuthorizationAllowance {
    private readonly subject;
    private readonly object;
    private readonly objectId;
    private readonly action;
    constructor(subject: any, object: any, objectId: any, action: any);
    isRequestAllowed(request: AuthorizationRequestInterface): boolean;
}
