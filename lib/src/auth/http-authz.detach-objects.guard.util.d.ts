import { ExecutionContext } from "@nestjs/common";
export declare class HttpAuthzDetachObjectsGuardUtil {
    private readonly context;
    private _util;
    constructor(context: ExecutionContext);
    isAuthorized(object: string, objectId: string, detachObject: string, detachObjectIds: string[], namespace?: string): boolean;
    get params(): any;
    get query(): any;
    get body(): any;
}
