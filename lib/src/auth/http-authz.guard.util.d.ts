import { ExecutionContext } from "@nestjs/common";
import { AuthorizationRequestInterface } from "./authorization-request.interface";
export declare class HttpAuthzGuardUtil {
    private readonly context;
    private _token;
    readonly params: any;
    readonly query: any;
    readonly body: any;
    constructor(context: ExecutionContext);
    isAuthorized(...authzRequests: AuthorizationRequestInterface[]): boolean;
    private _doesScopeAuthorizeRequest;
}
