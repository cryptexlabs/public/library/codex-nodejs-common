"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(require("./authenticator.interface"), exports);
__exportStar(require("./authf.guard"), exports);
__exportStar(require("./topic-authorizor.interface"), exports);
__exportStar(require("./fake-authenticator"), exports);
__exportStar(require("./http-authz.guard.util"), exports);
__exportStar(require("./http-authz.attach-objects.guard.util"), exports);
__exportStar(require("./http-authz.detach-objects.guard.util"), exports);
__exportStar(require("./http-authz.action-to-sub-objects.guard.util"), exports);
__exportStar(require("./http-authz.list-sub-objects.guard.util"), exports);
__exportStar(require("./authf-authenticator"), exports);
__exportStar(require("./authf-authenticator.options.interface"), exports);
//# sourceMappingURL=index.js.map