import { DefaultConfig } from "../config";
import { CanActivate, ExecutionContext } from "@nestjs/common";
import { ContextBuilder } from "../context";
export declare class AuthfGuard implements CanActivate {
    private readonly config;
    private readonly contextBuilder;
    private _pem;
    private _jwk;
    private _blacklistedSubjects;
    constructor(config: DefaultConfig, contextBuilder: ContextBuilder);
    canActivate(context: ExecutionContext): boolean;
    private _getApiHeaders;
    private _ensurePem;
    private _watchBlacklist;
    private _updateBlacklist;
}
