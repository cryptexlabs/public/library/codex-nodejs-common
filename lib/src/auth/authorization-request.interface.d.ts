export declare class AuthorizationRequestInterface {
    object: string;
    objectId: string | number;
    action: string;
}
