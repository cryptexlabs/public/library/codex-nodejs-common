"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpAuthzDetachObjectsGuardUtil = void 0;
const http_authz_action_to_sub_objects_guard_util_1 = require("./http-authz.action-to-sub-objects.guard.util");
class HttpAuthzDetachObjectsGuardUtil {
    constructor(context) {
        this.context = context;
        this._util = new http_authz_action_to_sub_objects_guard_util_1.HttpAuthzActionToSubObjectsGuardUtil(context, "delete");
    }
    isAuthorized(object, objectId, detachObject, detachObjectIds, namespace) {
        return this._util.isAuthorized(object, objectId, detachObject, detachObjectIds, namespace);
    }
    get params() {
        return this._util.params;
    }
    get query() {
        return this._util.query;
    }
    get body() {
        return this._util.body;
    }
}
exports.HttpAuthzDetachObjectsGuardUtil = HttpAuthzDetachObjectsGuardUtil;
//# sourceMappingURL=http-authz.detach-objects.guard.util.js.map