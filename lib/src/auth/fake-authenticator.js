"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FakeAuthenticator = void 0;
class FakeAuthenticator {
    authenticate(token) {
        return Promise.resolve(true);
    }
}
exports.FakeAuthenticator = FakeAuthenticator;
//# sourceMappingURL=fake-authenticator.js.map