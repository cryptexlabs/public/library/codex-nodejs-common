"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthfGuard = void 0;
const config_1 = require("../config");
const common_1 = require("@nestjs/common");
const axios_1 = require("axios");
const uuid_1 = require("uuid");
const context_1 = require("../context");
const jwkToPem = require("jwk-to-pem");
const jwt = require("jsonwebtoken");
let AuthfGuard = class AuthfGuard {
    constructor(config, contextBuilder) {
        this.config = config;
        this.contextBuilder = contextBuilder;
        if (config.authEnabled) {
            this._ensurePem().then();
            this._watchBlacklist().then();
        }
        this._blacklistedSubjects = [];
    }
    canActivate(context) {
        var _a;
        if (!this.config.authEnabled) {
            return true;
        }
        if (!this._pem) {
            throw new Error(`Pem not initialized!`);
        }
        const request = context.switchToHttp().getRequest();
        if (!((_a = request === null || request === void 0 ? void 0 : request.headers) === null || _a === void 0 ? void 0 : _a.authorization)) {
            throw new common_1.UnauthorizedException(`Invalid token`);
        }
        const token = request.headers.authorization.replace("Bearer ", "");
        let decodedToken;
        try {
            if (!token || token.split(".").length !== 3) {
                throw new common_1.UnauthorizedException("Malformed token: Not a valid JWT");
            }
            jwt.verify(token, this._pem, {
                algorithms: [this._jwk.alg],
            });
            decodedToken = jwt.decode(token);
        }
        catch (err) {
            if (err.name === "TokenExpiredError") {
                throw new common_1.UnauthorizedException("Token has expired");
            }
            else if (err.name === "JsonWebTokenError") {
                throw new common_1.UnauthorizedException(`Invalid token: ${err.message}`);
            }
            else if (err instanceof common_1.UnauthorizedException) {
                throw err;
            }
            else {
                throw new common_1.UnauthorizedException("Token verification failed");
            }
        }
        if (this._blacklistedSubjects.includes(decodedToken.sub)) {
            throw new common_1.UnauthorizedException();
        }
        return true;
    }
    _getApiHeaders(context) {
        const now = new Date();
        return {
            "X-Correlation-Id": (0, uuid_1.v4)(),
            "Accept-Language": "en-US",
            "X-Started": now.toISOString(),
            "X-Created": now.toISOString(),
            "X-Context-Category": "default",
            "X-Context-Id": "none",
            "X-Client-Name": context.client.name,
            "X-Client-Version": context.client.version,
            "X-Client-Variant": context.client.variant,
            "X-Client-Id": context.client.id,
        };
    }
    async _ensurePem() {
        if (this._pem) {
            return;
        }
        const context = this.contextBuilder.build().getResult();
        const keysHttpResponse = await axios_1.default.get(`/api/v1/key`, {
            baseURL: this.config.authfUrl,
            headers: this._getApiHeaders(context),
        });
        const response = keysHttpResponse.data;
        this._jwk = response.keys[0];
        this._pem = jwkToPem(this._jwk);
        context.logger.debug(`Got jwk from authf`);
    }
    async _watchBlacklist() {
        const context = this.contextBuilder.build().getResult();
        setInterval(async () => {
            try {
                await this._updateBlacklist(context);
            }
            catch (e) {
                context.logger.warn(`Failed to update subject blacklist: ${e.message}`);
            }
        }, this.config.blacklistRefreshIntervalInSeconds * 1000);
    }
    async _updateBlacklist(context) {
        var _a, _b, _c, _d, _e, _f;
        const headers = this._getApiHeaders(context);
        context.logger.verbose(`Fetching blacklist`);
        const subjectBlacklistResponse = await axios_1.default.get(`/api/v1/blacklisted-subjects`, {
            baseURL: this.config.authfUrl,
            headers,
        });
        context.logger.verbose(`Blacklist response`, subjectBlacklistResponse.data);
        if (((_a = subjectBlacklistResponse.data) === null || _a === void 0 ? void 0 : _a.data) !== undefined &&
            Array.isArray((_b = subjectBlacklistResponse.data) === null || _b === void 0 ? void 0 : _b.data)) {
            if (this._blacklistedSubjects.length !== ((_c = subjectBlacklistResponse.data) === null || _c === void 0 ? void 0 : _c.data)) {
                context.logger.debug(`Got change in blacklist`, {
                    old: this._blacklistedSubjects,
                    new: (_d = subjectBlacklistResponse.data) === null || _d === void 0 ? void 0 : _d.data,
                });
            }
            else if (this._blacklistedSubjects.length > 0) {
                context.logger.verbose(`Got blacklist`, (_e = subjectBlacklistResponse.data) === null || _e === void 0 ? void 0 : _e.data);
            }
            this._blacklistedSubjects = (_f = subjectBlacklistResponse.data) === null || _f === void 0 ? void 0 : _f.data;
        }
    }
};
exports.AuthfGuard = AuthfGuard;
exports.AuthfGuard = AuthfGuard = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("CONFIG")),
    __param(1, (0, common_1.Inject)("CONTEXT_BUILDER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig,
        context_1.ContextBuilder])
], AuthfGuard);
//# sourceMappingURL=authf.guard.js.map