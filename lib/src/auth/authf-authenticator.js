"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthfAuthenticator = void 0;
const axios_1 = require("axios");
const uuid_1 = require("uuid");
class AuthfAuthenticator {
    constructor(authfUrl, client, options) {
        this.authfUrl = authfUrl;
        this.client = client;
        this.options = options;
    }
    async authenticate(token) {
        var _a;
        if (((_a = this.options) === null || _a === void 0 ? void 0 : _a.authEnabled) === false) {
            return true;
        }
        const now = new Date();
        try {
            await axios_1.default.get(`/api/v1/authenticated`, {
                baseURL: this.authfUrl,
                headers: {
                    "X-Correlation-Id": (0, uuid_1.v4)(),
                    "Accept-Language": "en-US",
                    "X-Started": now.toISOString(),
                    "X-Created": now.toISOString(),
                    "X-Context-Category": "default",
                    "X-Context-Id": "none",
                    "X-Client-Name": this.client.name,
                    "X-Client-Version": this.client.version,
                    "X-Client-Variant": this.client.variant,
                    "X-Client-Id": this.client.id,
                    Authorization: `Bearer ${token}`,
                },
            });
            return true;
        }
        catch (e) {
            return false;
        }
    }
}
exports.AuthfAuthenticator = AuthfAuthenticator;
//# sourceMappingURL=authf-authenticator.js.map