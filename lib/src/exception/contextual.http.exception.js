"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextualHttpException = void 0;
const common_1 = require("@nestjs/common");
class ContextualHttpException extends common_1.HttpException {
    constructor(context, response, status) {
        super(response, status);
        this.context = context;
    }
}
exports.ContextualHttpException = ContextualHttpException;
//# sourceMappingURL=contextual.http.exception.js.map