"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotImplementedError = void 0;
class NotImplementedError extends Error {
    constructor(clazzMethod) {
        super(`${clazzMethod} not implemented`);
    }
    static throw() {
        throw this.make();
    }
    static make() {
        try {
            throw new Error();
        }
        catch (e) {
            const stack = e.stack || "";
            const stackLines = stack.split("\n");
            const callerLine = stackLines[3];
            const match = callerLine.match(/at (\S+)/);
            if (match && match.length > 1) {
                return new NotImplementedError(match[1]);
            }
            else {
                return new NotImplementedError("Unknown");
            }
        }
    }
}
exports.NotImplementedError = NotImplementedError;
//# sourceMappingURL=not-implemented.error.js.map