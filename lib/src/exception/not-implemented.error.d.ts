export declare class NotImplementedError extends Error {
    private constructor();
    static throw<T>(): T;
    static make(): NotImplementedError;
}
