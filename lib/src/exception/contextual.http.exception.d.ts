import { HttpException, HttpStatus } from "@nestjs/common";
import { Context } from "../context";
export declare class ContextualHttpException extends HttpException {
    readonly context: Context;
    constructor(context: Context, response: string | Record<string, any>, status: HttpStatus);
}
