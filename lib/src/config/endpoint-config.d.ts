import { HostConfig } from "./host-config";
import { EndpointConfigInterface } from "./endpoint-config.interface";
export declare class EndpointConfig extends HostConfig implements EndpointConfigInterface {
    constructor(host: any, port: string);
    get endpoint(): any;
}
