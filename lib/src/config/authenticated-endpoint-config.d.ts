import { AuthenticatedEndpointConfigInterface } from "./authenticated-endpoint-config.interface";
import { EndpointConfig } from "./endpoint-config";
export declare class AuthenticatedEndpointConfig extends EndpointConfig implements AuthenticatedEndpointConfigInterface {
    readonly username: string;
    readonly password: string;
    constructor(host: string, port: string, username: string, password: string);
    get endpoint(): string;
}
