"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_LOG_LEVELS = void 0;
exports.DEFAULT_LOG_LEVELS = {
    trace: 10,
    verbose: 10,
    debug: 20,
    info: 30,
    warn: 40,
    error: 50,
    fatal: 60,
};
//# sourceMappingURL=logging-config.interface.js.map