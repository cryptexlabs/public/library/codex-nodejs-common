"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultConfig = void 0;
const metrics_host_config_1 = require("./metrics-host-config");
const fs = require("fs");
const dotenv_1 = require("dotenv");
const kafka_config_1 = require("./kafka-config");
const jwt_config_1 = require("./jwt-config");
const common_1 = require("@nestjs/common");
const elasticsearch_config_1 = require("./elasticsearch-config");
const consumer_type_enum_1 = require("./consumer-type.enum");
const logging_config_interface_1 = require("./logging-config.interface");
const path_1 = require("path");
const yaml = require("js-yaml");
const glob = require("glob");
const watch = require("watch");
const fastq = require("fastq");
const logger_1 = require("../logger");
const process = require("process");
const mergeAnything = require("@cryptexlabs/merge-anything");
let DefaultConfig = class DefaultConfig {
    constructor(basePath, appPath, _clientId, fallbackEnvironment) {
        this._clientId = _clientId;
        this._basePath = basePath;
        this._appPath = appPath;
        this._envName = process.env.ENV_NAME || fallbackEnvironment || "";
        this._logger = new logger_1.CustomLogger("info", this.logLevels);
        this.setup(basePath, appPath, this._envName);
        this._configUpdateQueue = fastq((path, cb) => {
            this._onConfigUpdateQueueMessage(path).then(() => {
                cb(null);
            });
        }, 1);
        this._monitors = [];
        this._pathsWatching = [];
    }
    setConfigChangeLoaderEventHandler(eventHandler) {
        this._configChangeLoaderEventHandler = eventHandler;
    }
    async _onConfigUpdateQueueMessage(path) {
        this._serverConfig = this._loadServerConfig(false);
        if (this._configChangeLoaderEventHandler) {
            await this._configChangeLoaderEventHandler();
        }
    }
    getServerConfig() {
        if (!this._serverConfig) {
            this._serverConfig = this._loadServerConfig(true);
        }
        return this._serverConfig;
    }
    _loadServerConfig(startWatchers) {
        const serverConfigPath = process.env.SERVER_CONFIG_PATH || `${this._appPath}/config/server.yaml`;
        let serverConfig = {};
        if (fs.existsSync(serverConfigPath)) {
            serverConfig = this._loadConfigPath(serverConfigPath);
            if (startWatchers) {
                this._startConfigPatchWatch(serverConfigPath);
            }
        }
        if (process.env.SERVER_CONFIG_PATTERN) {
            serverConfig = this._loadConfigPatterns(serverConfig, startWatchers);
        }
        this._logger.debug(`Server Config`, { serverConfig });
        return serverConfig;
    }
    _startConfigPatchWatch(path) {
        if (this._hotReload) {
            this._logger.debug(`Starting watch for path ${path}`);
            const parent = (0, path_1.dirname)(path);
            if (!this._monitors.includes(parent)) {
                this._monitors.push(parent);
                this._logger.debug(`Creating monitor for directory ${parent}`);
                watch.createMonitor((0, path_1.dirname)(path), {}, (monitor) => {
                    monitor.on("changed", (f, curr, prev) => {
                        if (typeof f === "string") {
                            if (this._pathsWatching.includes(f)) {
                                this._logger.debug(`Config changed for path ${f}. Queueing update`);
                                this._configUpdateQueue.push(f);
                            }
                        }
                    });
                });
            }
            if (!this._pathsWatching.includes(path)) {
                this._pathsWatching.push(path);
            }
        }
    }
    _loadConfigPath(serverConfigPath) {
        const updatedServerConfig = yaml.load(fs.readFileSync(serverConfigPath).toString());
        if (typeof updatedServerConfig !== "object") {
            throw new Error(`Invalid config in file ${serverConfigPath} with env variable SERVER_CONFIG_PATH or default path: ${this._appPath}/config/server.yaml`);
        }
        return updatedServerConfig;
    }
    _loadConfigPatterns(serverConfig, startWatchers) {
        const paths = glob.sync(process.env.SERVER_CONFIG_PATTERN);
        this._logger.debug(`paths`, { paths });
        let serverConfigUpdated = serverConfig;
        for (const path of paths) {
            if (!path.includes(".yaml")) {
                throw new Error(`Invalid path ${path} found for pattern ${process.env.SERVER_CONFIG_PATTERN} with env variable SERVER_CONFIG_PATTERN`);
            }
            const c = yaml.load(fs.readFileSync(path).toString());
            if (typeof c !== "object") {
                throw new Error(`Invalid config in file ${path}`);
            }
            serverConfigUpdated = mergeAnything.merge(c, serverConfigUpdated);
            if (startWatchers) {
                this._startConfigPatchWatch(path);
            }
        }
        return serverConfigUpdated;
    }
    get _hotReload() {
        return process.env.SERVER_CONFIG_HOT_RELOAD !== "false";
    }
    toJSON() {
        return {
            fallbackLanguage: this.fallbackLanguage,
            fallbackCountry: this.fallbackCountry,
            appVersion: this.appVersion,
            appName: this.appName,
            clientId: this.clientId,
            apiVersion: this.apiVersion,
            appPrefix: this.appPrefix,
            docsPrefix: this.docsPrefix,
            docsEnabled: this.docsEnabled,
            docsPath: this.docsPath,
            kafka: this.kafka,
            environmentName: this.environmentName,
            logLevels: this.logLevels,
            httpPort: this.httpPort,
            metrics: this.metrics,
            jwt: this.jwt,
            healthzFilePath: this.healthzFilePath,
            elasticsearch: this.elasticsearch,
            consumerType: this.consumerType,
            gpuEnabled: this.gpuEnabled,
            dataDirectory: this.dataDirectory,
            serviceName: this.serviceName,
            logLevel: this.logLevel,
            loggerType: this.loggerType,
            telemetry: this.telemetry,
        };
    }
    setup(basePath, appPath, environment) {
        const envBasePath = `${basePath}/env`;
        const configPath = `${envBasePath}/${environment}.env`;
        if (fs.existsSync(configPath)) {
            (0, dotenv_1.config)({ path: configPath });
        }
        const secretsPath = `${envBasePath}/${environment}${environment ? "." : ""}secrets.env`;
        if (fs.existsSync(secretsPath)) {
            const envConfig = (0, dotenv_1.parse)(fs.readFileSync(secretsPath));
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        const configOverridePath = `${envBasePath}/${environment}.override.env`;
        if (fs.existsSync(configOverridePath)) {
            const envConfig = (0, dotenv_1.parse)(fs.readFileSync(configOverridePath));
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        const packageJson = require(`${appPath}/package.json`);
        this._appName = packageJson.name;
        this._packageVersion = packageJson.version;
    }
    addEnvFile(fileName) {
        const envBasePath = `${this._basePath}/env`;
        const envConfigPath = `${envBasePath}/${this._envName}.${fileName}.env`;
        if (fs.existsSync(envConfigPath)) {
            const envConfig = (0, dotenv_1.parse)(fs.readFileSync(envConfigPath));
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        return this;
    }
    get fallbackLanguage() {
        return process.env.LOCALE_LANGUAGE_FALLBACK || "en";
    }
    get fallbackCountry() {
        return process.env.LOCALE_COUNTRY_FALLBACK || "US";
    }
    get appVersion() {
        return process.env.APP_VERSION || this._packageVersion;
    }
    get appName() {
        return this._appName;
    }
    get clientId() {
        return this._clientId;
    }
    get apiVersion() {
        return process.env.API_VERSION || "v1";
    }
    get appPrefix() {
        return process.env.APP_PREFIX || "api";
    }
    get docsPrefix() {
        return process.env.DOCS_PREFIX || "docs";
    }
    get docsEnabled() {
        return process.env.DOCS_ENABLED === "true";
    }
    get docsPath() {
        return process.env.DOCS_PATH || "";
    }
    get kafka() {
        return new kafka_config_1.KafkaConfig(process.env.KAFKA_ENABLED, process.env.KAFKA_HOST, process.env.KAFKA_PORT, process.env.KAFKA_USERNAME, process.env.KAFKA_PASSWORD, process.env.SASL_MECHANISM, process.env.KAFKA_TOPICS_CONFIG_PATH ||
            `${this._appPath}/config/topics.yaml`, process.env.KAFKA_CLIENT_ID || process.env.POD_NAME || this.appName, process.env.KAFKA_DEFAULT_PARTITIONS, process.env.KAFKA_DEFAULT_ERROR_TOPIC);
    }
    get serviceName() {
        return process.env.SERVICE_NAME;
    }
    get environmentName() {
        return process.env.ENV_NAME;
    }
    get logLevels() {
        return (process.env.LOG_LEVELS || "debug,info,error").trim().split(",");
    }
    get loggerType() {
        return process.env.LOGGER_TYPE || "default";
    }
    get logLevel() {
        return this.logLevels.reduce((prev, curr) => {
            return logging_config_interface_1.DEFAULT_LOG_LEVELS[curr] < logging_config_interface_1.DEFAULT_LOG_LEVELS[prev]
                ? curr
                : prev;
        }, "info");
    }
    get httpPort() {
        return parseInt(process.env.HTTP_PORT || "3000", 10);
    }
    get externalHttpPort() {
        return process.env.EXTERNAL_HTTP_PORT
            ? parseInt(process.env.EXTERNAL_HTTP_PORT, 10)
            : this.httpPort;
    }
    get metrics() {
        return new metrics_host_config_1.MetricsHostConfig(process.env.GRAPHITE_HOST, process.env.GRAPHITE_PORT, process.env.METRICS_ENABLED);
    }
    get telemetry() {
        return {
            metrics: {
                enabled: process.env.TELEMETRY_METRICS_ENABLED === "true",
                path: process.env.TELEMETRY_METRICS_PATH,
                port: parseInt(process.env.TELEMETRY_METRICS_PORT || "8081", 10),
            },
            traces: {
                enabled: process.env.TELEMETRY_TRACE_ENABLED === "true",
                traceUrl: process.env.TELEMETRY_TRACE_URL || "http://localhost:4317",
            },
        };
    }
    get jwt() {
        return new jwt_config_1.JwtConfig(process.env.JWT_VERIFY, process.env.JWT_AUD);
    }
    get healthzFilePath() {
        return process.env.HEALTHZ_FILE_PATH || "/tmp/healthz";
    }
    get elasticsearch() {
        return new elasticsearch_config_1.ElasticsearchConfig(process.env.ELASTICSEARCH_URL || "http://elasticsearch:9200", process.env.ELASTICSEARCH_PING_INTERVAL_SECONDS || "10");
    }
    get consumerType() {
        return (process.env.CONSUMER_TYPE ||
            consumer_type_enum_1.ConsumerTypeEnum.DEFAULT);
    }
    get gpuEnabled() {
        return process.env.GPU_ENABLED === "true";
    }
    get dataDirectory() {
        return process.env.DATA_DIRECTORY || "/data";
    }
    get overrideOrganization() {
        return process.env.OVERRIDE_ORGANIZATION;
    }
    getMetaType(type) {
        return (this.overrideOrganization
            ? type.replace(/^cryptexlabs\./, `${this.overrideOrganization}.`)
            : type);
    }
    get apiPrefixes() {
        return process.env.API_PREFIXES
            ? process.env.API_PREFIXES.split(",")
            : [this.appPrefix];
    }
    get prettyPrintLogs() {
        return process.env.PRETTY_PRINT_LOGS !== "false";
    }
    get redis() {
        return {
            enabled: process.env.REDIS_ENABLED === "true",
            host: process.env.REDIS_HOST ? process.env.REDIS_HOST : "localhost",
            port: parseInt(process.env.REDIS_PORT ? process.env.REDIS_PORT : "6379", 10),
        };
    }
    get pagination() {
        return {
            defaultPageSize: parseInt(process.env.PAGINATION_DEFAULT_PAGESIZE || "20", 10),
            maxPageSize: parseInt(process.env.PAGINATION_MAX_PAGESIZE || "200", 10),
        };
    }
    get sql() {
        return {
            database: process.env.SQL_DATABASE,
            client: process.env.SQL_CLIENT,
            userName: process.env.SQL_USERNAME,
            password: process.env.SQL_PASSWORD,
            primary: {
                hostName: process.env.SQL_HOSTNAME,
                port: parseInt(process.env.SQL_PORT, 10),
                pool: {
                    min: parseInt(process.env.SQL_POOL_MIN, 10),
                    max: parseInt(process.env.SQL_POOL_MAX, 10),
                },
            },
            reader: {
                hostName: process.env.SQL_HOSTNAME_RO,
                port: parseInt(process.env.SQL_PORT_RO, 10),
                pool: {
                    min: parseInt(process.env.SQL_POOL_RO_MIN, 10),
                    max: parseInt(process.env.SQL_POOL_RO_MAX, 10),
                },
            },
        };
    }
    get authfUrl() {
        return process.env.AUTHF_URL || "http://localhost:3007";
    }
    get authEnabled() {
        return (process.env.AUTH_ENABLED === "true" ||
            !["localhost", "docker"].includes(this.environmentName));
    }
    get blacklistRefreshIntervalInSeconds() {
        return process.env.BLACKLIST_REFRESH_INTERVAL_IN_SECONDS
            ? parseInt(process.env.BLACKLIST_REFRESH_INTERVAL_IN_SECONDS, 10)
            : 15;
    }
};
exports.DefaultConfig = DefaultConfig;
exports.DefaultConfig = DefaultConfig = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [String, String, String, String])
], DefaultConfig);
//# sourceMappingURL=default-config.js.map