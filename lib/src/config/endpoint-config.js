"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EndpointConfig = void 0;
const host_config_1 = require("./host-config");
class EndpointConfig extends host_config_1.HostConfig {
    constructor(host, port) {
        super(host, port);
    }
    get endpoint() {
        if (!this.host.includes(",")) {
            return `${this.host}:${this.port}`;
        }
        else {
            return this.host
                .split(",")
                .map((h) => `${h}:${this.port}`)
                .join(",");
        }
    }
}
exports.EndpointConfig = EndpointConfig;
//# sourceMappingURL=endpoint-config.js.map