import { SwitchConfigInterface } from "./switch-config.interface";
import { HostConfigInterface } from "./host-config.interface";
export interface RedisConfigInterface extends HostConfigInterface, SwitchConfigInterface {
}
