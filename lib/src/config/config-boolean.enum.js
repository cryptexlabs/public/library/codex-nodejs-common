"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigBooleanEnum = void 0;
var ConfigBooleanEnum;
(function (ConfigBooleanEnum) {
    ConfigBooleanEnum["TRUE"] = "true";
    ConfigBooleanEnum["FALSE"] = "false";
})(ConfigBooleanEnum || (exports.ConfigBooleanEnum = ConfigBooleanEnum = {}));
//# sourceMappingURL=config-boolean.enum.js.map