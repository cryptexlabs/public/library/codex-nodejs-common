import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class FloatValidationPipe implements PipeTransform {
    private _schema;
    constructor();
    transform(value: any, metadata: ArgumentMetadata): any;
}
