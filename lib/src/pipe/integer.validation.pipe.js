"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegerValidationPipe = void 0;
const common_1 = require("@nestjs/common");
let IntegerValidationPipe = class IntegerValidationPipe {
    transform(value, metadata) {
        if (parseInt(value, 10).toString() !== value.toString()) {
            throw new common_1.BadRequestException(`${value} must be an integer`);
        }
        return value;
    }
};
exports.IntegerValidationPipe = IntegerValidationPipe;
exports.IntegerValidationPipe = IntegerValidationPipe = __decorate([
    (0, common_1.Injectable)()
], IntegerValidationPipe);
//# sourceMappingURL=integer.validation.pipe.js.map