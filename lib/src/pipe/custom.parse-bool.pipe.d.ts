import { PipeTransform } from "@nestjs/common";
export declare class CustomParseBoolPipe implements PipeTransform<string | undefined | boolean | number, boolean> {
    private readonly options?;
    constructor(options?: {
        required?: boolean;
    });
    transform(value: string | undefined | boolean | number): boolean | undefined;
}
