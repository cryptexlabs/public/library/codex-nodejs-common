"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextLogger = void 0;
const uuid_1 = require("uuid");
class ContextLogger {
    constructor(correlationId, config, client, logger) {
        this.correlationId = correlationId;
        this.config = config;
        this.client = client;
        this.logger = logger;
        this.stackTraceId = (0, uuid_1.v4)();
    }
    debug(message, data) {
        this.logger.debug(message, this._getData(data));
    }
    error(message, data) {
        this.logger.error(message, this._getData(data));
    }
    log(message, data) {
        this.logger.log(message, this._getData(data));
    }
    verbose(message, data) {
        this.logger.verbose(message, this._getData(data));
    }
    warn(message, data) {
        this.logger.warn(message, this._getData(data));
    }
    _getData(data) {
        let logData = {
            stackTraceId: this.stackTraceId,
            correlationId: this.correlationId,
            appVersion: this.config.appVersion,
            appName: this.config.appName,
            clientName: this.client.name,
            clientVersion: this.client.version,
            clientVariant: this.client.variant,
        };
        if (data) {
            logData = Object.assign(Object.assign({}, logData), { data });
        }
        return logData;
    }
}
exports.ContextLogger = ContextLogger;
//# sourceMappingURL=context.logger.js.map