import { ConsoleLogger } from "@nestjs/common";
import { DefaultConfig } from "../config";
declare type LogLevel = "log" | "error" | "warn" | "debug" | "verbose" | "fatal";
export declare class CustomConsoleLogger extends ConsoleLogger {
    private readonly config;
    private static _lastTimestampAt?;
    constructor(config: DefaultConfig);
    protected printMessages(messages: unknown[], context?: string, logLevel?: LogLevel, writeStreamType?: "stdout" | "stderr"): void;
    private _updateAndGetTimestampDiff;
    private _getColorByLogLevel;
}
export {};
