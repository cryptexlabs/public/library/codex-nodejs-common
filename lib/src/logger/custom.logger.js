"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomLogger = void 0;
const common_1 = require("@nestjs/common");
let CustomLogger = class CustomLogger {
    constructor(defaultContext = "info", filterContext = [], isTimestampEnabled, logger) {
        this.defaultContext = defaultContext;
        this.filterContext = filterContext;
        if (logger) {
            this.logger = logger;
        }
        else {
            this.logger = new common_1.Logger(defaultContext, {
                timestamp: isTimestampEnabled,
            });
        }
    }
    log(message, ...optionalParams) {
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("log")) {
                this.logger.log(message, ...optionalParams);
            }
        }
        else {
            this.logger.log(message, ...optionalParams);
        }
    }
    error(error, ...optionalParams) {
        let message;
        let useTrace;
        if (error instanceof Error) {
            message = error.message;
            useTrace = error.stack;
        }
        else if (typeof error === "object" && error.stack) {
            message = error;
            useTrace = error.stack;
        }
        if (!message && error.message) {
            message = error.message;
        }
        if (!useTrace && error.stack) {
            useTrace = error.stack;
        }
        if (!message && typeof error === "string") {
            message = error;
        }
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("error")) {
                if (useTrace) {
                    this.logger.error(message, Object.assign({ trace: useTrace }, optionalParams));
                }
                else {
                    this.logger.error(message, ...optionalParams);
                }
            }
        }
        else {
            if (useTrace) {
                this.logger.error(message, Object.assign({ trace: useTrace }, optionalParams));
            }
            else {
                this.logger.error(message, ...optionalParams);
            }
        }
    }
    debug(message, ...optionalParams) {
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("debug")) {
                this.logger.debug(message, ...optionalParams);
            }
        }
        else {
            this.logger.debug(message, ...optionalParams);
        }
    }
    verbose(message, ...optionalParams) {
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("verbose")) {
                this.logger.verbose(message, ...optionalParams);
            }
        }
        else {
            this.logger.verbose(message, ...optionalParams);
        }
    }
    warn(message, ...optionalParams) {
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("warn")) {
                this.logger.warn(message, ...optionalParams);
            }
        }
        else {
            this.logger.warn(message, ...optionalParams);
        }
    }
    info(message) {
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes("info")) {
                this.logger.log(message, "info");
            }
        }
        else {
            this.logger.log(message, "info");
        }
    }
};
exports.CustomLogger = CustomLogger;
exports.CustomLogger = CustomLogger = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [String, Array, Boolean, Object])
], CustomLogger);
//# sourceMappingURL=custom.logger.js.map