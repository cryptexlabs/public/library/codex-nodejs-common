"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtendedCodexMetaType = void 0;
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
exports.ExtendedCodexMetaType = Object.assign(Object.assign({}, codex_data_model_1.CodexMetaTypeEnum), { get MY_TYPE() {
        return "extended.my-type";
    } });
//# sourceMappingURL=extended.codex-meta-type.js.map