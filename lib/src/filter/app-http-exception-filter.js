"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppHttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const response_1 = require("../response");
const exception_1 = require("../exception");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const locales_1 = require("../locales/locales");
const enum_1 = require("../locales/enum");
const config_1 = require("../config");
const exception_2 = require("../exception");
const logger_1 = require("../logger");
const util_1 = require("../util");
let AppHttpExceptionFilter = class AppHttpExceptionFilter extends core_1.BaseExceptionFilter {
    constructor(fallbackLocale, fallbackLogger, config, applicationRef) {
        super(applicationRef);
        this.fallbackLocale = fallbackLocale;
        this.fallbackLogger = fallbackLogger;
        this.config = config;
    }
    isDebugLevel() {
        return this.config.logLevels.some((level) => level.toLowerCase() === "debug");
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        let fallbackLogger = this.fallbackLogger;
        let fallbackLocale = this.fallbackLocale;
        if (request.headers["Accept-Language"]) {
            try {
                fallbackLocale = util_1.LocaleUtil.getLocaleFromAcceptLanguageHeader(null, request.headers["Accept-Language"]);
            }
            catch (e) {
                this.fallbackLogger.error(`Could not parse 'Accept-Language' header: '${request.headers["Accept-Language"]}'`);
            }
        }
        if (request.headers["x-correlation-id"]) {
            fallbackLogger = new logger_1.ContextLogger(request.headers["x-correlation-id"], this.config, {
                id: request.headers["x-client-id"],
                version: request.headers["x-client-version"],
                name: request.headers["x-client-name"],
                variant: request.headers["x-client-variant"],
            }, this.fallbackLogger);
        }
        const logger = exception instanceof exception_1.FriendlyHttpException ||
            exception instanceof exception_2.ContextualHttpException
            ? exception.context.logger
            : fallbackLogger;
        let developerText = this.getDeveloperText(exception, fallbackLocale);
        let stack = "No stack available. Very sad";
        if (exception.stack) {
            if (typeof exception.stack === "string") {
                stack = exception.stack.split("\n").map((line) => line.trim());
            }
            else if (Array.isArray(exception.stack)) {
                stack = exception.stack;
            }
            else if (typeof exception.stack === "object") {
            }
            else {
                logger.error("Strange Stack", exception.stack);
            }
        }
        else if (typeof exception === "object" && exception.name) {
            logger.error(`No stack available for error with name ${exception.name}`);
        }
        else {
            logger.error(`No stack available for error of type ${typeof exception}`);
        }
        if (developerText) {
            if (this.config.loggerType === "pino") {
                logger.error(developerText, { body: request.body, stack });
            }
            else {
                logger.error(developerText, {
                    request: {
                        headers: request.headers,
                        body: request.body,
                        url: request.url,
                        method: request.method,
                        stack,
                    },
                });
            }
        }
        else {
            logger.error([
                "Error not explained",
                "This probably means someone wrote code that throws an error without an error message",
                "This should never happen!",
            ].join(". "), {
                stack,
            });
        }
        if (!(exception instanceof common_1.HttpException ||
            exception instanceof exception_1.FriendlyHttpException ||
            exception instanceof exception_2.ContextualHttpException) &&
            exception.stack) {
            logger.error(exception.stack);
        }
        const status = exception.getStatus !== undefined &&
            typeof exception.getStatus === "function"
            ? exception.getStatus()
            : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        const locale = exception instanceof exception_1.FriendlyHttpException ||
            exception instanceof exception_2.ContextualHttpException
            ? exception.context.locale
            : fallbackLocale;
        const correlationId = exception instanceof exception_1.FriendlyHttpException ||
            exception instanceof exception_2.ContextualHttpException
            ? exception.context.correlationId
            : request.headers["x-correlation-id"];
        const started = exception instanceof exception_1.FriendlyHttpException ||
            exception instanceof exception_2.ContextualHttpException
            ? exception.context.started
            : request.headers["x-started"]
                ? new Date(request.headers["x-started"])
                : undefined;
        developerText = this.getDeveloperText(exception, locale);
        const userMessage = exception.userMessage ? exception.userMessage : "";
        const path = request.url || request.headers["x-forwarded-uri"];
        const errorHttpResponse = new response_1.ErrorHttpResponse(status, locale, new response_1.ErrorMessage(locale, locales_1.i18nData, null, null, this.isDebugLevel()
            ? developerText
            : "Error unavailable in this context", userMessage), this.isDebugLevel() ? exception.stack || null : null, this.config, correlationId, started, path);
        if (errorHttpResponse.meta.context) {
            if (errorHttpResponse.meta.context.id &&
                request.headers["x-context-id"]) {
                errorHttpResponse.meta.context.id = request.headers["x-context-id"];
            }
            if (errorHttpResponse.meta.context.category &&
                request.headers["x-context-category"]) {
                errorHttpResponse.meta.context.category = request.headers["x-context-category"];
            }
        }
        response.status(status).json(errorHttpResponse);
    }
    getDeveloperText(exception, locale) {
        let developerText = locales_1.i18nData.__({
            phrase: enum_1.LocalesEnum.UNKNOWN_ERROR,
            locale: locale.i18n,
        });
        if (exception) {
            if (exception.getStatus) {
                if (exception.response && exception.response.message) {
                    if (Array.isArray(exception.response.message)) {
                        developerText = exception.response.message.join(" + ");
                    }
                    else {
                        developerText = exception.response.message;
                    }
                }
                else if (exception.message.message) {
                    developerText = exception.message.message;
                }
                else if (exception.getResponse) {
                    developerText = exception.getResponse();
                }
                else {
                    developerText = exception.message;
                }
            }
            else if (exception.message) {
                developerText = exception.message;
            }
        }
        return developerText;
    }
};
exports.AppHttpExceptionFilter = AppHttpExceptionFilter;
exports.AppHttpExceptionFilter = AppHttpExceptionFilter = __decorate([
    (0, common_1.Catch)(),
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)("LOGGER")),
    __metadata("design:paramtypes", [codex_data_model_1.Locale, Object, config_1.DefaultConfig, Object])
], AppHttpExceptionFilter);
//# sourceMappingURL=app-http-exception-filter.js.map