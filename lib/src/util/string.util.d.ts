export declare class StringUtil {
    private constructor();
    static isRegexString(str: string): boolean;
    static stringMatches(test: string, str: string): boolean;
    static insecureMd5(str: string): string;
    static insecureSha1(str: string): string;
    static sha256(str: string): string;
    static sha512(str: string): string;
    static byteArray(str: string): number[];
    static isValidUUIDv4(uuid: any): boolean;
}
