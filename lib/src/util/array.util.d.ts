export declare class ArrayUtil {
    static paginate(array: any, pageNumber: any, pageSize: any): any;
    static unique(array: any): any;
    static intersection(array1: any, array2: any): any;
}
