"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayUtil = void 0;
class ArrayUtil {
    static paginate(array, pageNumber, pageSize) {
        return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
    }
    static unique(array) {
        return array.filter((v, i, a) => a.indexOf(v) === i);
    }
    static intersection(array1, array2) {
        return array1.filter((value) => array2.includes(value));
    }
}
exports.ArrayUtil = ArrayUtil;
//# sourceMappingURL=array.util.js.map