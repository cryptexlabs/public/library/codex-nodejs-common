"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateUtil = void 0;
const moment_1 = require("moment");
class DateUtil {
    static isValidISO8601Date(dateString) {
        const regex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(.\d+)?(Z|[+-]\d{2}:\d{2})$/;
        return regex.test(dateString);
    }
    static fromMysqlStringToISOString(date) {
        if (!date) {
            return date;
        }
        return ((0, moment_1.default)(date, "YYYY-MM-DD HH:mm:ss").toISOString(true).slice(0, 23) + "Z");
    }
    static toMysqlString(date) {
        if (!date) {
            return date;
        }
        return (0, moment_1.default)(date, "YYYY-MM-DDThh:mm:ssZ")
            .utcOffset(0)
            .format("YYYY-MM-DD HH:mm:ss");
    }
}
exports.DateUtil = DateUtil;
//# sourceMappingURL=date.util.js.map