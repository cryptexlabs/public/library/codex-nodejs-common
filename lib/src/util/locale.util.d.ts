import { ApiMetaHeadersInterface, Locale } from "@cryptexlabs/codex-data-model";
import { Context } from "../context";
export declare class LocaleUtil {
    static getLocaleFromAcceptLanguageHeader(context: Context, langCode: string): Locale;
    static getLocaleFromHeaders(headers: ApiMetaHeadersInterface, context: Context): Locale;
}
