"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocaleUtil = void 0;
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const common_1 = require("@nestjs/common");
const exception_1 = require("../exception");
const locales_1 = require("../locales/locales");
class LocaleUtil {
    static getLocaleFromAcceptLanguageHeader(context, langCode) {
        if (!langCode) {
            throw new exception_1.FriendlyHttpException(`Accept language header is not set`, context, locales_1.i18nData.__({ phrase: "an-error-occurred", locale: "en-US" }), common_1.HttpStatus.BAD_REQUEST, "LocaleUtil\ngetLocaleFromAcceptLanguageHeader");
        }
        const parts = langCode.split("-");
        let language;
        let country;
        if (parts.length !== 2) {
            const parts2 = langCode.split(";");
            if (parts2.length === 2) {
                const parts3 = parts2[0].split(",");
                if (parts3.length === 2) {
                    language = parts3[1].toLocaleLowerCase().trim();
                    country = parts3[0].toUpperCase().trim();
                }
            }
            if (!language || !country) {
                throw new exception_1.FriendlyHttpException(`Invalid Accept-Language header: ${langCode}`, context, locales_1.i18nData.__({ phrase: "language-not-supported", locale: "en-US" }), common_1.HttpStatus.BAD_REQUEST, "LocaleUtil\ngetLocaleFromAcceptLanguageHeader");
            }
        }
        else {
            language = parts[0].toLocaleLowerCase().trim();
            country = parts[1].toUpperCase().trim();
            const parts4 = country.split(";");
            if (parts4.length === 2) {
                const parts5 = parts4[0].split(",");
                if (parts5.length === 2) {
                    country = parts5[0].toUpperCase().trim();
                }
            }
        }
        country = country.split(",")[0];
        if (!codex_data_model_1.ValidLanguageCodes.includes(language)) {
            throw new exception_1.FriendlyHttpException(`Invalid language code: ${country}`, context, locales_1.i18nData.__({ phrase: "language-not-supported", locale: "en-US" }), common_1.HttpStatus.NOT_ACCEPTABLE, "LocaleUtil\ngetLocaleFromAcceptLanguageHeader");
        }
        if (!codex_data_model_1.ValidCountryCodes.includes(country)) {
            throw new exception_1.FriendlyHttpException(`Invalid country code: ${country}`, context, locales_1.i18nData.__({ phrase: "language-not-supported", locale: "en-US" }), common_1.HttpStatus.NOT_ACCEPTABLE, "LocaleUtil\ngetLocaleFromAcceptLanguageHeader");
        }
        return new codex_data_model_1.Locale(language, country);
    }
    static getLocaleFromHeaders(headers, context) {
        const langCode = headers["accept-language"] || headers["Accept-Language"];
        return this.getLocaleFromAcceptLanguageHeader(context, langCode);
    }
}
exports.LocaleUtil = LocaleUtil;
//# sourceMappingURL=locale.util.js.map