export declare class DateUtil {
    static isValidISO8601Date(dateString: string): boolean;
    static fromMysqlStringToISOString(date: string | Date): string;
    static toMysqlString(date: string | Date): string;
}
