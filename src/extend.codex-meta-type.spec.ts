import { CodexMetaTypeEnum } from "@cryptexlabs/codex-data-model";
import { ExtendedCodexMetaType } from "./extended.codex-meta-type";

describe("Extend Codex Meta Type", () => {
  it("should be extensible!", () => {
    function example(type: CodexMetaTypeEnum) {
      return type;
    }

    expect(example(ExtendedCodexMetaType.MY_TYPE)).toEqual(
      ExtendedCodexMetaType.MY_TYPE
    );
  });
});
