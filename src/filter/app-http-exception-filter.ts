import {
  ArgumentsHost,
  Catch,
  HttpException,
  HttpServer,
  HttpStatus,
  Inject,
  Injectable,
  LoggerService,
} from "@nestjs/common";
import { BaseExceptionFilter } from "@nestjs/core";
import { Request } from "express";
import { ErrorHttpResponse, ErrorMessage } from "../response";
import { FriendlyHttpException } from "../exception";
import { Locale } from "@cryptexlabs/codex-data-model";
import { i18nData } from "../locales/locales";
import { LocalesEnum } from "../locales/enum";
import { DefaultConfig } from "../config";
import { ContextualHttpException } from "../exception";
import { ContextLogger } from "../logger";
import { LocaleUtil } from "../util";

@Catch()
@Injectable()
export class AppHttpExceptionFilter extends BaseExceptionFilter {
  constructor(
    private readonly fallbackLocale: Locale,
    @Inject("LOGGER") private readonly fallbackLogger: LoggerService,
    private readonly config: DefaultConfig,
    applicationRef?: HttpServer
  ) {
    super(applicationRef);
  }

  private isDebugLevel(): boolean {
    return this.config.logLevels.some(
      (level) => level.toLowerCase() === "debug"
    );
  }

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse();

    let fallbackLogger = this.fallbackLogger;
    let fallbackLocale = this.fallbackLocale;

    if (request.headers["Accept-Language"]) {
      try {
        fallbackLocale = LocaleUtil.getLocaleFromAcceptLanguageHeader(
          null,
          request.headers["Accept-Language"] as string | undefined
        );
      } catch (e) {
        this.fallbackLogger.error(
          `Could not parse 'Accept-Language' header: '${request.headers["Accept-Language"]}'`
        );
      }
    }

    if (request.headers["x-correlation-id"]) {
      fallbackLogger = new ContextLogger(
        request.headers["x-correlation-id"],
        this.config,
        {
          id: request.headers["x-client-id"] as string | undefined,
          version: request.headers["x-client-version"] as string | undefined,
          name: request.headers["x-client-name"] as string | undefined,
          variant: request.headers["x-client-variant"] as string | undefined,
        },
        this.fallbackLogger
      );
    }

    const logger =
      exception instanceof FriendlyHttpException ||
      exception instanceof ContextualHttpException
        ? exception.context.logger
        : fallbackLogger;

    let developerText = this.getDeveloperText(exception, fallbackLocale);
    let stack = "No stack available. Very sad";

    if (exception.stack) {
      if (typeof exception.stack === "string") {
        stack = exception.stack.split("\n").map((line) => line.trim());
      } else if (Array.isArray(exception.stack)) {
        stack = exception.stack;
      } else if (typeof exception.stack === "object") {
      } else {
        logger.error("Strange Stack", exception.stack);
      }
    } else if (typeof exception === "object" && exception.name) {
      logger.error(`No stack available for error with name ${exception.name}`);
    } else {
      logger.error(`No stack available for error of type ${typeof exception}`);
    }

    if (developerText) {
      if (this.config.loggerType === "pino") {
        logger.error(developerText, { body: request.body, stack });
      } else {
        logger.error(developerText, {
          request: {
            headers: request.headers,
            body: request.body,
            url: request.url,
            method: request.method,
            stack,
          },
        });
      }
    } else {
      logger.error(
        [
          "Error not explained",
          "This probably means someone wrote code that throws an error without an error message",
          "This should never happen!",
        ].join(". "),
        {
          stack,
        }
      );
    }

    // If this is a normal error it means something random occurred and we want the stacktrace
    if (
      !(
        exception instanceof HttpException ||
        exception instanceof FriendlyHttpException ||
        exception instanceof ContextualHttpException
      ) &&
      exception.stack
    ) {
      logger.error(exception.stack);
    }

    const status =
      exception.getStatus !== undefined &&
      typeof exception.getStatus === "function"
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const locale =
      exception instanceof FriendlyHttpException ||
      exception instanceof ContextualHttpException
        ? exception.context.locale
        : fallbackLocale;

    const correlationId =
      exception instanceof FriendlyHttpException ||
      exception instanceof ContextualHttpException
        ? exception.context.correlationId
        : (request.headers["x-correlation-id"] as string | undefined);

    const started =
      exception instanceof FriendlyHttpException ||
      exception instanceof ContextualHttpException
        ? exception.context.started
        : request.headers["x-started"]
        ? new Date(request.headers["x-started"] as string)
        : undefined;

    developerText = this.getDeveloperText(exception, locale);

    const userMessage = exception.userMessage ? exception.userMessage : "";
    const path: string =
      request.url || (request.headers["x-forwarded-uri"] as string);

    const errorHttpResponse = new ErrorHttpResponse(
      status,
      locale,
      new ErrorMessage(
        locale,
        i18nData,
        null,
        null,
        this.isDebugLevel()
          ? developerText
          : "Error unavailable in this context",
        userMessage
      ),
      this.isDebugLevel() ? exception.stack || null : null,
      this.config,
      correlationId,
      started,
      path
    );

    // TODO remove hack
    if (errorHttpResponse.meta.context) {
      if (
        errorHttpResponse.meta.context.id &&
        request.headers["x-context-id"]
      ) {
        errorHttpResponse.meta.context.id = request.headers[
          "x-context-id"
        ] as string;
      }
      if (
        errorHttpResponse.meta.context.category &&
        request.headers["x-context-category"]
      ) {
        errorHttpResponse.meta.context.category = request.headers[
          "x-context-category"
        ] as string;
      }
    }

    response.status(status).json(errorHttpResponse);
  }

  getDeveloperText(exception: any, locale: any): string {
    let developerText: string = i18nData.__({
      phrase: LocalesEnum.UNKNOWN_ERROR,
      locale: locale.i18n,
    });
    if (exception) {
      if (exception.getStatus) {
        if (exception.response && exception.response.message) {
          if (Array.isArray(exception.response.message)) {
            developerText = exception.response.message.join(" + ");
          } else {
            developerText = exception.response.message;
          }
        } else if (exception.message.message) {
          developerText = exception.message.message;
        } else if (exception.getResponse) {
          developerText = exception.getResponse();
        } else {
          developerText = exception.message;
        }
      } else if (exception.message) {
        developerText = exception.message;
      }
    }
    return developerText;
  }
}
