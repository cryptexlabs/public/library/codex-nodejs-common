import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  registerDecorator,
  ValidationOptions,
} from "class-validator";
import { EmailTypeEnum } from "@cryptexlabs/authf-data-model";

@ValidatorConstraint({ name: "IsValidEmailType", async: false })
export class IsValidEmailTypeConstraint
  implements ValidatorConstraintInterface {
  private readonly EMAIL_TYPES: string[] = Object.values(EmailTypeEnum);

  validate(timezone: any, args: ValidationArguments): boolean {
    return this.EMAIL_TYPES.includes(timezone);
  }

  defaultMessage(args: ValidationArguments): string {
    return `"${args.value}" is not a valid UTC timezone. Choose one of the allowed timezones.`;
  }
}

export function IsValidEmailType(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsValidEmailTypeConstraint,
    });
  };
}
