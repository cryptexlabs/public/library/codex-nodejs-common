import { ApiQuery } from "@nestjs/swagger";
import { applyDecorators } from "@nestjs/common";

export function ApiQueryOptions() {
  return applyDecorators(
    ...[
      ApiQuery({
        name: "orderBy",
        required: false,
        schema: {
          example: "name",
        },
      }),
      ApiQuery({
        name: "orderDirection",
        required: false,
        schema: {
          example: "asc",
        },
      }),
      ApiQuery({
        name: "searchName",
        required: false,
      }),
      ApiQuery({
        name: "search",
        required: false,
      }),
    ]
  );
}
