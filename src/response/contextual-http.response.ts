import { HttpResponse } from "./http-response";
import { HttpStatus } from "@nestjs/common";
import { JsonSerializableInterface } from "../message";
import {
  CodexMetaTypeEnum,
  MessageInterface,
} from "@cryptexlabs/codex-data-model";
import { Context } from "../context";
import { I18n } from "i18n";

export class ContextualHttpResponse<T>
  extends HttpResponse<T>
  implements JsonSerializableInterface<MessageInterface<T>> {
  private readonly _message: string;

  constructor(
    context: Context,
    status: HttpStatus,
    i18nInstance: I18n,
    phraseKey: string
  ) {
    super(
      status,
      context.config.getMetaType(CodexMetaTypeEnum.NA_HTTP_SIMPLE),
      context.locale,
      context.config,
      context.correlationId,
      context.started,
      context.path
    );
    this.meta.context = context.messageContext
      ? context.messageContext
      : this.meta.context;
    this._message = i18nInstance.__({
      phrase: phraseKey,
      locale: context.locale.i18n,
    });
  }

  public get data(): string {
    return this._message;
  }
}
