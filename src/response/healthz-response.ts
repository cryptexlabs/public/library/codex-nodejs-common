import { HttpStatus } from "@nestjs/common";
import {
  CodexMetaTypeEnum,
  LocaleI18nInterface,
  MessageInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { v4 as uuidV4 } from "uuid";
import { i18nData } from "../locales/locales";
import { LocalesEnum } from "../locales/enum";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";
import { HttpResponseMeta } from "./http-response-meta";

export class HealthzResponse
  implements JsonSerializableInterface<MessageInterface<string>> {
  private readonly _phraseKey: LocalesEnum;
  public readonly meta: MessageMetaInterface;

  constructor(
    status: HttpStatus,
    private locale: LocaleI18nInterface,
    config: DefaultConfig
  ) {
    this.meta = new HttpResponseMeta(
      status,
      config.getMetaType(CodexMetaTypeEnum.NA_HTTP_HEALTHZ),
      locale,
      config,
      uuidV4(),
      undefined,
      undefined
    );

    if (status >= 200 && status < 400) {
      this._phraseKey = LocalesEnum.I_AM_HEALTHY;
    } else if (status >= 400 && status < 500) {
      this._phraseKey = LocalesEnum.INVALID_REQUEST;
    } else {
      this._phraseKey = LocalesEnum.UNKNOWN_ERROR;
    }
  }

  public toJSON(): MessageInterface<string> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }

  public get data(): string {
    return i18nData.__({ phrase: this._phraseKey, locale: this.locale.i18n });
  }
}
