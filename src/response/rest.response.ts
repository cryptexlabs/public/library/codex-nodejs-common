import { HttpStatus } from "@nestjs/common";
import {
  MessageInterface,
  MessageMetaHttpInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
import { HttpResponseMeta } from "./http-response-meta";

export class RestResponse<T>
  implements JsonSerializableInterface<MessageInterface<T>> {
  public readonly meta: MessageMetaHttpInterface;

  constructor(
    context: Context,
    status: HttpStatus,
    type: string,
    public readonly data: T,
    public readonly path?: string
  ) {
    this.meta = new HttpResponseMeta(
      status,
      type,
      context.locale,
      context.config,
      context.correlationId,
      context.started,
      context.path
    ) as MessageMetaInterface;
    this.meta.context = context.messageContext;
  }

  public toJSON(): MessageInterface<T> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }
}
