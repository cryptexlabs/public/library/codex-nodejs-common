import { HttpStatus } from "@nestjs/common";
import {
  CodexMetaTypeEnum,
  ErrorMessageInterface,
  LocaleInterface,
  MessageInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { ErrorMessageDataInterface } from "./error-message-data.interface";
import { JsonSerializableInterface } from "../message";
import { HttpResponseMeta } from "./http-response-meta";

export class ErrorHttpResponse
  implements
    JsonSerializableInterface<MessageInterface<ErrorMessageDataInterface>> {
  public readonly meta: MessageMetaInterface;

  constructor(
    status: HttpStatus,
    locale: LocaleInterface,
    private readonly errorMessage: ErrorMessageInterface,
    private readonly stack: string,
    config: DefaultConfig,
    correlationId?: string,
    started?: Date,
    path?: string
  ) {
    this.meta = new HttpResponseMeta(
      status,
      config.getMetaType(CodexMetaTypeEnum.NA_HTTP_ERROR),
      locale,
      config,
      correlationId,
      started,
      path
    );
  }

  public toJSON(): MessageInterface<ErrorMessageDataInterface> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }

  public get data(): ErrorMessageDataInterface {
    return {
      message: this.errorMessage,
      // Don't send stack trace on non-production environment to client
      stack: process.env.NODE_ENV !== "production" ? this.stack : null,
    };
  }
}
