import { HttpStatus } from "@nestjs/common";
import {
  LocaleInterface,
  MessageInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";
import { HttpResponseMeta } from "./http-response-meta";

export abstract class HttpResponse<T>
  implements
    JsonSerializableInterface<MessageInterface<T>>,
    MessageInterface<T> {
  public meta: HttpResponseMeta;

  abstract get data();

  protected constructor(
    private status: HttpStatus,
    private type: string,
    locale: LocaleInterface,
    config: DefaultConfig,
    correlationId?: string,
    started?: Date,
    path?: string
  ) {
    this.meta = new HttpResponseMeta(
      status,
      type,
      locale,
      config,
      correlationId,
      started,
      path
    );
  }

  public toJSON(): MessageInterface<T> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }
}
