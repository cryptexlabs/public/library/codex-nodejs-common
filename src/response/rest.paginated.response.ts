import { HttpStatus } from "@nestjs/common";
import {
  MessageInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
import { HttpPaginatedResponseMeta } from "./http-paginated-response-meta";

export class RestPaginatedResponse<L>
  implements JsonSerializableInterface<MessageInterface<L[]>> {
  public readonly meta: MessageMetaInterface;

  constructor(
    context: Context,
    status: HttpStatus,
    totalRecords: number,
    type: string,
    public readonly data: L[]
  ) {
    this.meta = new HttpPaginatedResponseMeta(
      status,
      totalRecords,
      type,
      context.locale,
      context.config,
      context.correlationId,
      context.started,
      context.path
    ) as MessageMetaInterface;
  }

  public toJSON(): MessageInterface<L[]> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }
}
