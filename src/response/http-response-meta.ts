import { HttpStatus } from "@nestjs/common";
import {
  LocaleInterface,
  MessageMetaHttpInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";

export class HttpResponseMeta
  extends MessageMeta
  implements MessageMetaHttpInterface {
  constructor(
    public readonly status: HttpStatus,
    type: string,
    locale: LocaleInterface,
    config: DefaultConfig,
    correlationId: string,
    started: Date,
    public readonly path: string
  ) {
    super(type, locale, config, correlationId, started);
  }

  public toJSON(): any {
    return {
      path: this.path,
      status: this.status,
      type: this.type,
      schemaVersion: this.schemaVersion,
      correlationId: this.correlationId,
      time: this.time,
      context: this.context,
      locale: this.locale,
      client: this.client,
    };
  }
}
