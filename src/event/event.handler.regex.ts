import { EventHandlerRegexInterface } from "./event.handler.regex.interface";
import { EventHandlerInterface } from "./event.handler.interface";

export class EventHandlerRegex implements EventHandlerRegexInterface {
  constructor(
    public readonly handler: EventHandlerInterface,
    public readonly regexp: RegExp
  ) {}
}
