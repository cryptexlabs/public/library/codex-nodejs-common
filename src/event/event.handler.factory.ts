import { EventHandlerInterface } from "./event.handler.interface";
import { EventHandlerFactoryInterface } from "./event.handler.factory.interface";
import { EventHandlerRegexInterface } from "./event.handler.regex.interface";

export abstract class EventHandlerFactory
  implements EventHandlerFactoryInterface {
  protected readonly handlerKeyValueMap: Record<string, EventHandlerInterface>;
  protected readonly handlerRegexList: EventHandlerRegexInterface[];

  protected constructor(
    handlerKeyValueMap: Record<string, EventHandlerInterface>,
    handlerRegexList: EventHandlerRegexInterface[]
  ) {
    this.handlerKeyValueMap = handlerKeyValueMap;
    this.handlerRegexList = handlerRegexList;
  }

  public getHandler(topic: string): EventHandlerInterface {
    for (const item of this.handlerRegexList) {
      if (item.regexp.test(topic)) {
        return item.handler;
      }
    }

    if (!this.handlerKeyValueMap[topic]) {
      throw new Error(`Unknown topic: ${topic}`);
    }

    return this.handlerKeyValueMap[topic];
  }
}
