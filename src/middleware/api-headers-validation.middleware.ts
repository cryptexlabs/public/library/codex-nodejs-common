import {
  BadRequestException,
  Inject,
  Injectable,
  NestMiddleware,
} from "@nestjs/common";
import { DefaultConfig } from "../config";
import { LocaleUtil, StringUtil } from "../util";
import { Context, ContextBuilder } from "../context";
import { DateUtil } from "../util/date.util";

@Injectable()
export class ApiHeadersValidationMiddleware implements NestMiddleware {
  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("CONTEXT_BUILDER") private readonly contextBuilder: ContextBuilder
  ) {}

  use(req: any, res: any, next: () => void): any {
    const requiredHeaders = {
      /**
       * Unique identifier generated when a person takes an action or a system time based event is triggered
       * This correlation id should be passed between systems and should be only created if none exists
       * Stops being re-used when an action is required from a person
       * Format is UUID V4
       */
      "x-correlation-id": (context: Context, correlationId: string): void => {
        if (correlationId.trim() === "") {
          throw new BadRequestException(
            `X-Correlation-Id header is empty. Correlation Id is Unique identifier generated when a person takes an action or a system time based event is triggered. This correlation id should be passed between systems and should be only created if none exists. Stops being re-used when an action is required from a person. Format is UUID V4`
          );
        }
        if (!StringUtil.isValidUUIDv4(correlationId)) {
          throw new BadRequestException(
            `Invalid X-Correlation-Id header. Must be UUID V4. For example: '57d7f725-420d-45b5-9f92-101c6c0cffc3'`
          );
        }
      },

      /**
       * ISO 639 alpha-1 language code
       * Every endpoint must be able to receive and pass along this property
       */
      "accept-language": (context: Context, acceptLanguage: string): void => {
        if (acceptLanguage.trim() === "") {
          throw new BadRequestException(
            `Accept-Language header is empty. For example 'en-US'`
          );
        }
        const parsedLanguage = LocaleUtil.getLocaleFromAcceptLanguageHeader(
          context,
          acceptLanguage
        );
        if (!parsedLanguage) {
          throw new BadRequestException(
            `Invalid Accept-Language header. Must be ISO 639 alpha-1 language code. Or standard language supported by a web browser. For example: 'en-US' or 'en-US,en;q=0.9'`
          );
        }
      },

      /**
       * UTC timestamp in ISO 8601 format for when the correlation id was created
       * This timestamp should be passed between systems and should be only created if none exists
       */
      "x-started": (context: Context, started: string): void => {
        if (started.trim() === "") {
          throw new BadRequestException(`X-Started header is empty`);
        }
        if (!DateUtil.isValidISO8601Date(started)) {
          throw new BadRequestException(
            `Invalid date format for X-Started header. Must be ISO 8601 format. For example '2024-02-29T19:47:57.366Z'`
          );
        }
      },

      /**
       * UTC timestamp in ISO 8601 format for when this particular message was created
       */
      "x-created": (context: Context, created: string): void => {
        if (created.trim() === "") {
          throw new BadRequestException(`X-Created header is empty`);
        }
        if (!DateUtil.isValidISO8601Date(created)) {
          throw new BadRequestException(
            `Invalid date format for X-Created header. Must be ISO 8601 format. For example '2024-02-29T19:47:57.366Z'`
          );
        }
      },

      /**
       * A category for the context of the request. For example 'test', 'performance test' or 'debug'
       */
      "x-context-category": (
        context: Context,
        contextCategory: string
      ): void => {
        if (contextCategory.trim() === "") {
          throw new BadRequestException(
            `X-Context-Category header is empty. A category for the context of the request. For example 'test', 'performance test' or 'debug'`
          );
        }
      },

      /**
       * A unique context identifier used for correlating logs and metrics usually for performance or experimental testing
       * UUID v4 format
       */
      "x-context-id": (context: Context, contextId: string): void => {
        if (contextId.trim() === "") {
          throw new BadRequestException(`X-Context-Id header is empty`);
        }
        if (!StringUtil.isValidUUIDv4(contextId) && contextId !== "none") {
          throw new BadRequestException(
            `Invalid X-Context-Id header. Must be UUID V4 or 'none'. A unique context identifier used for correlating logs and metrics usually for performance or experimental testing`
          );
        }
      },

      /**
       * A unique identifier for the client to help identify exactly which application or third party is making the requests
       * Must be UUID V4 format
       */
      "x-client-id": (context: Context, clientId: string): void => {
        if (clientId.trim() === "") {
          throw new BadRequestException(
            `X-Client-Id header is empty. A unique identifier for the client to help identify exactly which application or third party is making the requests. Should be identical for every client. Used to uniquely identify projects. You can hard code this string in your project. Its not a secret. Should NOT be different for every request.`
          );
        }
        if (!StringUtil.isValidUUIDv4(clientId)) {
          throw new BadRequestException(
            `Invalid X-Client-Id header. Must be UUID V4 format`
          );
        }
      },

      /**
       * Canonical name of the client that is making the API call. Such as "BlueBerry Picker App"
       */
      "x-client-name": (context: Context, clientName: string): void => {
        if (clientName.trim() === "") {
          throw new BadRequestException(
            `X-Client-Name header is empty. Canonical name of the client that is making the API call. Such as "BlueBerry Picker App"`
          );
        }
      },

      /**
       * Version of the client that is making the API call. Such as '1.3.8'
       */
      "x-client-version": (context: Context, clientVersion: string): void => {
        if (clientVersion.trim() === "") {
          throw new BadRequestException(
            `X-Client-Version header is empty. Version of the client that is making the API call. Such as '1.3.8'`
          );
        }
      },

      /**
       * Variation of the client build. There can be different builds of the same version of a client such as 'dev', 'test' or 'demo'
       * Typically these variations would be configured to interact with different backends and have different debugging levels
       */
      "x-client-variant": (context: Context, clientVariant: string): void => {
        if (clientVariant.trim() === "") {
          throw new BadRequestException(
            "X-Client-Variant header is Empty. Variation of the client build. There can be different builds of the same version of a client such as 'dev', 'test' or 'demo'. Typically these variations would be configured to interact with different backends and have different debugging levels"
          );
        }
      },
    };

    const optionalHeaders = {
      /**
       * Override the servers understanding of what "now" is. Allows you to go forward or backwards in time.
       * Optional
       */
      "x-now": (context: Context, now: string): void => {
        try {
          if (now) {
            new Date(now);
          }
        } catch (e) {
          throw new BadRequestException(`X-Now header is invalid: '${now}'`);
        }
      },
    };

    if (
      this.config.apiPrefixes.some((prefix) => {
        return req.path.startsWith(`/${prefix}`);
      })
    ) {
      const missingHeaders = Object.keys(requiredHeaders).filter(
        (header) => !req.headers[header.toLowerCase()]
      );

      if (missingHeaders.length > 0) {
        throw new BadRequestException(
          `Missing required headers: ${missingHeaders.join(", ")}`
        );
      }

      const validationContext = this.contextBuilder
        .build()
        .setMetaFromHeaders(req.headers)
        .getResult();

      for (const headerName of Object.keys(requiredHeaders)) {
        const validator = requiredHeaders[headerName];
        const header = req.headers[headerName];
        validator(validationContext, header);
      }

      for (const headerName of Object.keys(optionalHeaders)) {
        const validator = optionalHeaders[headerName];
        const header = req.headers[headerName];
        validator(validationContext, header);
      }
    }

    next();
  }
}
