import { Injectable, NestMiddleware } from "@nestjs/common";

@Injectable()
export class ForwardedUriMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void): any {
    req.headers["x-forwarded-uri"] = req.url;
    next();
  }
}
