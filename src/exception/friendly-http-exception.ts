import { HttpException, HttpStatus } from "@nestjs/common";
import { Context } from "../context";

export class FriendlyHttpException extends HttpException {
  constructor(
    response: string | Record<string, any>,
    public readonly context: Context,
    public readonly userMessage: string,
    status: HttpStatus,

    public readonly stack: string
  ) {
    super(response, status);
  }
}
