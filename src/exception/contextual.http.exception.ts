import { HttpException, HttpStatus } from "@nestjs/common";
import { Context } from "../context";

export class ContextualHttpException extends HttpException {
  constructor(
    public readonly context: Context,
    response: string | Record<string, any>,
    status: HttpStatus
  ) {
    super(response, status);
  }
}
