import { ConsoleLogger, Inject, Injectable } from "@nestjs/common";
import { isPlainObject } from "@nestjs/common/utils/shared.utils";
import { clc, yellow } from "@nestjs/common/utils/cli-colors.util";
import { DefaultConfig } from "../config";

declare type LogLevel =
  | "log"
  | "error"
  | "warn"
  | "debug"
  | "verbose"
  | "fatal";

// This assumes extension of ConsoleLogger from @nestjs/common@8.0.6
@Injectable()
export class CustomConsoleLogger extends ConsoleLogger {
  private static _lastTimestampAt?: number;

  constructor(@Inject("CONFIG") private readonly config: DefaultConfig) {
    super();
  }

  protected printMessages(
    messages: unknown[],
    context = "",
    logLevel: LogLevel = "log",
    writeStreamType?: "stdout" | "stderr"
  ) {
    const space = this.config.prettyPrintLogs ? 2 : 0;

    const color = this._getColorByLogLevel(logLevel);
    messages.forEach((message) => {
      const output = isPlainObject(message)
        ? `${color("Object:")}\n${JSON.stringify(
            message,
            (key, value) =>
              typeof value === "bigint" ? value.toString() : value,
            space
          )}\n`
        : color(message as string);

      const pidMessage = color(`[Nest] ${process.pid}  - `);
      const contextMessage = context ? yellow(`[${context}] `) : "";
      const timestampDiff = this._updateAndGetTimestampDiff();
      const formattedLogLevel = color(logLevel.toUpperCase().padStart(7, " "));
      const computedMessage = `${pidMessage}${this.getTimestamp()} ${formattedLogLevel} ${contextMessage}${output}${timestampDiff}\n`;

      process[writeStreamType ?? "stdout"].write(computedMessage);
    });
  }

  private _updateAndGetTimestampDiff(): string {
    const includeTimestamp =
      CustomConsoleLogger._lastTimestampAt && this.options?.timestamp;
    const result = includeTimestamp
      ? yellow(` +${Date.now() - CustomConsoleLogger._lastTimestampAt}ms`)
      : "";
    CustomConsoleLogger._lastTimestampAt = Date.now();
    return result;
  }

  private _getColorByLogLevel(level: LogLevel) {
    switch (level) {
      case "debug":
        return clc.magentaBright;
      case "warn":
        return clc.yellow;
      case "error":
        return clc.red;
      case "verbose":
        return clc.cyanBright;
      default:
        return clc.green;
    }
  }
}
