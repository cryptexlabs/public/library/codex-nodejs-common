import { ExecutionContext } from "@nestjs/common";
import { HttpAuthzActionToSubObjectsGuardUtil } from "./http-authz.action-to-sub-objects.guard.util";

/**
 * Authorizes detachment of objects to another object by object id
 */
export class HttpAuthzDetachObjectsGuardUtil {
  private _util: HttpAuthzActionToSubObjectsGuardUtil;

  constructor(private readonly context: ExecutionContext) {
    this._util = new HttpAuthzActionToSubObjectsGuardUtil(context, "delete");
  }

  /**
   * @param {string} object The object name of object A
   * @param {string} objectId The object ID of object A
   * @param {string} detachObject The object name of objects B
   * @param {string[]} detachObjectIds The object IDs of Objects B to attach to object A
   * @param {string?} namespace (Optional) The namespace of objects A and B
   */
  public isAuthorized(
    object: string,
    objectId: string,
    detachObject: string,
    detachObjectIds: string[],
    namespace?: string
  ) {
    return this._util.isAuthorized(
      object,
      objectId,
      detachObject,
      detachObjectIds,
      namespace
    );
  }

  public get params() {
    return this._util.params;
  }

  public get query() {
    return this._util.query;
  }

  public get body() {
    return this._util.body;
  }
}
