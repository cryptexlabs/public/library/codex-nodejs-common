import { DefaultConfig } from "../config";
import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";
import {
  JwtKeyInterface,
  JwtKeyResponseInterface,
} from "@cryptexlabs/authf-data-model";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";
import { Context, ContextBuilder } from "../context";
import * as jwkToPem from "jwk-to-pem";
import * as jwt from "jsonwebtoken";
import { Algorithm } from "jsonwebtoken";

@Injectable()
export class AuthfGuard implements CanActivate {
  private _pem: string;
  private _jwk: JwtKeyInterface;
  private _blacklistedSubjects: string[];

  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("CONTEXT_BUILDER") private readonly contextBuilder: ContextBuilder
  ) {
    if (config.authEnabled) {
      this._ensurePem().then();
      this._watchBlacklist().then();
    }
    this._blacklistedSubjects = [];
  }

  public canActivate(context: ExecutionContext): boolean {
    if (!this.config.authEnabled) {
      return true;
    }

    if (!this._pem) {
      throw new Error(`Pem not initialized!`);
    }

    const request = context.switchToHttp().getRequest();

    if (!request?.headers?.authorization) {
      throw new UnauthorizedException(`Invalid token`);
    }
    const token = request.headers.authorization.replace("Bearer ", "");

    let decodedToken: any;

    try {
      // Check if the token has three parts separated by a dot
      if (!token || token.split(".").length !== 3) {
        throw new UnauthorizedException("Malformed token: Not a valid JWT");
      }

      // Proceed to verify the token
      jwt.verify(token, this._pem, {
        algorithms: [this._jwk.alg as Algorithm],
      });

      decodedToken = (jwt.decode(token) as unknown) as any;
    } catch (err) {
      if (err.name === "TokenExpiredError") {
        throw new UnauthorizedException("Token has expired");
      } else if (err.name === "JsonWebTokenError") {
        throw new UnauthorizedException(`Invalid token: ${err.message}`);
      } else if (err instanceof UnauthorizedException) {
        throw err; // Re-throw the malformed token error
      } else {
        throw new UnauthorizedException("Token verification failed");
      }
    }

    if (this._blacklistedSubjects.includes(decodedToken.sub)) {
      throw new UnauthorizedException();
    }

    return true;
  }

  private _getApiHeaders(context: Context): Record<string, string> {
    const now = new Date();
    return {
      "X-Correlation-Id": uuidv4(),
      "Accept-Language": "en-US",
      "X-Started": now.toISOString(),
      "X-Created": now.toISOString(),
      "X-Context-Category": "default",
      "X-Context-Id": "none",
      "X-Client-Name": context.client.name,
      "X-Client-Version": context.client.version,
      "X-Client-Variant": context.client.variant,
      "X-Client-Id": context.client.id,
    };
  }

  private async _ensurePem(): Promise<void> {
    if (this._pem) {
      return;
    }
    const context = this.contextBuilder.build().getResult();

    const keysHttpResponse = await axios.get(`/api/v1/key`, {
      baseURL: this.config.authfUrl as string,
      headers: this._getApiHeaders(context) as any,
    });

    const response: JwtKeyResponseInterface = keysHttpResponse.data;

    this._jwk = response.keys[0];
    this._pem = jwkToPem(this._jwk as any);

    context.logger.debug(`Got jwk from authf`);
  }

  private async _watchBlacklist() {
    const context = this.contextBuilder.build().getResult();

    setInterval(async () => {
      try {
        await this._updateBlacklist(context);
      } catch (e) {
        context.logger.warn(`Failed to update subject blacklist: ${e.message}`);
      }
    }, this.config.blacklistRefreshIntervalInSeconds * 1000);
  }

  private async _updateBlacklist(context: Context) {
    const headers = this._getApiHeaders(context);

    context.logger.verbose(`Fetching blacklist`);
    const subjectBlacklistResponse = await axios.get(
      `/api/v1/blacklisted-subjects`,
      {
        baseURL: this.config.authfUrl,
        headers,
      }
    );
    context.logger.verbose(`Blacklist response`, subjectBlacklistResponse.data);

    if (
      subjectBlacklistResponse.data?.data !== undefined &&
      Array.isArray(subjectBlacklistResponse.data?.data)
    ) {
      if (
        this._blacklistedSubjects.length !== subjectBlacklistResponse.data?.data
      ) {
        context.logger.debug(`Got change in blacklist`, {
          old: this._blacklistedSubjects,
          new: subjectBlacklistResponse.data?.data,
        });
      } else if (this._blacklistedSubjects.length > 0) {
        context.logger.verbose(
          `Got blacklist`,
          subjectBlacklistResponse.data?.data
        );
      }
      this._blacklistedSubjects = subjectBlacklistResponse.data?.data;
    }
  }
}
