import { AuthenticatorInterface } from "./authenticator.interface";
import { ClientInterface } from "@cryptexlabs/codex-data-model";
import { AuthfAuthenticatorOptionsInterface } from "./authf-authenticator.options.interface";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";

export class AuthfAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly authfUrl: string,
    private readonly client: ClientInterface,
    private readonly options?: AuthfAuthenticatorOptionsInterface
  ) {}

  public async authenticate(token: string): Promise<boolean> {
    if (this.options?.authEnabled === false) {
      return true;
    }

    const now = new Date();

    try {
      await axios.get(`/api/v1/authenticated`, {
        baseURL: this.authfUrl,
        headers: {
          "X-Correlation-Id": uuidv4(),
          "Accept-Language": "en-US",
          "X-Started": now.toISOString(),
          "X-Created": now.toISOString(),
          "X-Context-Category": "default",
          "X-Context-Id": "none",
          "X-Client-Name": this.client.name,
          "X-Client-Version": this.client.version,
          "X-Client-Variant": this.client.variant,
          "X-Client-Id": this.client.id,
          Authorization: `Bearer ${token}`,
        },
      });

      return true;
    } catch (e) {
      return false;
    }
  }
}
