import { LocaleUtil } from "./locale.util";
import { HttpException } from "@nestjs/common";
import { FriendlyHttpException } from "../exception";
import {
  ApiMetaHeadersInterface,
  CountryLanguageCombination,
} from "@cryptexlabs/codex-data-model";

describe("LocaleUtil", () => {
  describe("getLocaleFromHeaders", () => {
    it("Should throw error when no accept language header is specified", () => {
      expect(() => {
        LocaleUtil.getLocaleFromHeaders({} as ApiMetaHeadersInterface, null);
      }).toThrow(HttpException);
    });

    it("Should parse a normal header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-US",
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse a lowercase header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-US",
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse a double language", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-US,us" as CountryLanguageCombination,
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an uppercase language code in header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "EN-US" as CountryLanguageCombination,
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse a lowercase country code in header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-us" as CountryLanguageCombination,
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an accept language in the format: 'US,EN;Q=0.9'", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "US,EN;Q=0.9" as CountryLanguageCombination,
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an accept language in the format: 'en-US,en;q=0.9'", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "    en-US,en;q=0.9" as CountryLanguageCombination,
        } as ApiMetaHeadersInterface,
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should throw an error for an invalid country code", () => {
      expect(() => {
        LocaleUtil.getLocaleFromHeaders(
          {
            "accept-language": "asldkafsdj-*" as CountryLanguageCombination,
          } as ApiMetaHeadersInterface,
          null
        );
      }).toThrow(FriendlyHttpException);
    });
  });
});
