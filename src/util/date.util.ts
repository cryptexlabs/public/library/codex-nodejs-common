import moment from "moment";

export class DateUtil {
  public static isValidISO8601Date(dateString: string) {
    const regex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(.\d+)?(Z|[+-]\d{2}:\d{2})$/;
    return regex.test(dateString);
  }

  static fromMysqlStringToISOString(date: string | Date): string {
    if (!date) {
      return date as any;
    }
    return (
      moment(date, "YYYY-MM-DD HH:mm:ss").toISOString(true).slice(0, 23) + "Z"
    );
  }

  static toMysqlString(date: string | Date): string {
    if (!date) {
      return date as any;
    }
    return moment(date, "YYYY-MM-DDThh:mm:ssZ")
      .utcOffset(0)
      .format("YYYY-MM-DD HH:mm:ss");
  }
}
