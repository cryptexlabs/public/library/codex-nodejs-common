import * as crypto from "crypto";

export class StringUtil {
  private constructor() {}
  public static isRegexString(str: string) {
    return str.substr(0, 1) === "/" && str.substr(str.length - 1, 1) === "/";
  }

  public static stringMatches(test: string, str: string) {
    const testTrimmed = test.trim();
    const strTrimmed = str.trim();
    if (test.trim() === str.trim()) {
      return true;
    }
    if (this.isRegexString(testTrimmed)) {
      if (
        new RegExp(testTrimmed.substr(1, testTrimmed.length - 2)).test(
          strTrimmed
        )
      ) {
        return true;
      }
    }
    return false;
  }

  public static insecureMd5(str: string) {
    return crypto.createHash("md5").update(str).digest("hex");
  }

  public static insecureSha1(str: string) {
    return crypto.createHash("sha1").update(str).digest("hex");
  }

  public static sha256(str: string) {
    return crypto.createHash("sha256").update(str).digest("hex");
  }

  public static sha512(str: string) {
    return crypto.createHash("sha512").update(str).digest("hex");
  }

  public static byteArray(str: string): number[] {
    return [...Buffer.from(str)];
  }

  public static isValidUUIDv4(uuid) {
    const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
    return regex.test(uuid);
  }
}
