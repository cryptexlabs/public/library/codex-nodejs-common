import {
  ApiMetaHeadersInterface,
  CountryCode,
  LanguageCode,
  Locale,
  ValidCountryCodes,
  ValidLanguageCodes,
} from "@cryptexlabs/codex-data-model";
import { HttpStatus } from "@nestjs/common";
import { FriendlyHttpException } from "../exception";
import { Context } from "../context";
import { i18nData } from "../locales/locales";

export class LocaleUtil {
  public static getLocaleFromAcceptLanguageHeader(
    context: Context,
    langCode: string
  ) {
    if (!langCode) {
      throw new FriendlyHttpException(
        `Accept language header is not set`,
        context,
        i18nData.__({ phrase: "an-error-occurred", locale: "en-US" }),
        HttpStatus.BAD_REQUEST,
        "LocaleUtil\ngetLocaleFromAcceptLanguageHeader"
      );
    }
    const parts = langCode.split("-");
    let language: string;
    let country: string;

    if (parts.length !== 2) {
      const parts2 = langCode.split(";");
      if (parts2.length === 2) {
        const parts3 = parts2[0].split(",");
        if (parts3.length === 2) {
          language = parts3[1].toLocaleLowerCase().trim();
          country = parts3[0].toUpperCase().trim();
        }
      }

      if (!language || !country) {
        throw new FriendlyHttpException(
          `Invalid Accept-Language header: ${langCode}`,
          context,
          i18nData.__({ phrase: "language-not-supported", locale: "en-US" }),
          HttpStatus.BAD_REQUEST,
          "LocaleUtil\ngetLocaleFromAcceptLanguageHeader"
        );
      }
    } else {
      language = parts[0].toLocaleLowerCase().trim();
      country = parts[1].toUpperCase().trim();
      const parts4 = country.split(";");
      if (parts4.length === 2) {
        const parts5 = parts4[0].split(",");
        if (parts5.length === 2) {
          country = parts5[0].toUpperCase().trim();
        }
      }
    }

    country = country.split(",")[0];

    if (!ValidLanguageCodes.includes(language)) {
      throw new FriendlyHttpException(
        `Invalid language code: ${country}`,
        context,
        i18nData.__({ phrase: "language-not-supported", locale: "en-US" }),
        HttpStatus.NOT_ACCEPTABLE,
        "LocaleUtil\ngetLocaleFromAcceptLanguageHeader"
      );
    }
    if (!ValidCountryCodes.includes(country)) {
      throw new FriendlyHttpException(
        `Invalid country code: ${country}`,
        context,
        i18nData.__({ phrase: "language-not-supported", locale: "en-US" }),
        HttpStatus.NOT_ACCEPTABLE,
        "LocaleUtil\ngetLocaleFromAcceptLanguageHeader"
      );
    }

    return new Locale(language as LanguageCode, country as CountryCode);
  }

  public static getLocaleFromHeaders(
    headers: ApiMetaHeadersInterface,
    context: Context
  ) {
    const langCode = headers["accept-language"] || headers["Accept-Language"];
    return this.getLocaleFromAcceptLanguageHeader(context, langCode);
  }
}
