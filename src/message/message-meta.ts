import { v4 as uuidV4 } from "uuid";
import {
  ClientInterface,
  LocaleInterface,
  MessageContextInterface,
  MessageMetaInterface,
  MetaTimeInterface,
  Version,
} from "@cryptexlabs/codex-data-model";
import { ServiceClient } from "../client";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "./json-serializable.interface";

export class MessageMeta
  implements
    MessageMetaInterface,
    JsonSerializableInterface<MessageMetaInterface> {
  //
  time: MetaTimeInterface;

  //
  context: MessageContextInterface;

  //
  interactionId: string | null;

  //
  schemaVersion: "0.1.0";

  private _correlationId: string;

  public get correlationId(): string {
    if (!this._correlationId) {
      this._correlationId = uuidV4();
    }
    return this._correlationId;
  }

  public client: ClientInterface;

  constructor(
    public readonly type: string,
    public readonly locale: LocaleInterface,
    config: DefaultConfig,
    correlationId?: string,
    private readonly started?: Date
  ) {
    const now = new Date(Date.now());
    const start = started || now;
    this.time = {
      created: now.toISOString(),
      started: start.toISOString(),
    };
    this.context = {
      category: "default",
      id: "none",
    };
    this.interactionId = null;
    this.schemaVersion = Version.CURRENT;
    this._correlationId = correlationId;
    this.client = new ServiceClient(config);
  }

  public toJSON(): MessageMetaInterface {
    return {
      type: this.type,
      schemaVersion: this.schemaVersion,
      correlationId: this.correlationId,
      time: this.time,
      context: this.context,
      locale: this.locale,
      client: this.client,
    };
  }
}
