import {
  ClientInterface,
  LocaleInterface,
  MessageContextInterface,
  MessageMetaInterface,
  MetaTimeInterface,
  Version,
} from "@cryptexlabs/codex-data-model";
import { Context } from "../context";

export class MessageMetaFromMeta implements MessageMetaInterface {
  public readonly client: ClientInterface;
  public readonly context: MessageContextInterface;
  public readonly correlationId: string;
  public readonly locale: LocaleInterface;
  public readonly schemaVersion: "0.1.0";
  public readonly time: MetaTimeInterface;

  constructor(
    context: Context,
    meta: MessageMetaInterface,
    public readonly type: string
  ) {
    this.client = context.client;
    this.context = context.messageContext;
    this.correlationId = meta.correlationId;
    this.locale = meta.locale;
    this.schemaVersion = Version.CURRENT;
    this.time = {
      started: meta.time.started,
      created: new Date().toISOString(),
    };
  }
}
