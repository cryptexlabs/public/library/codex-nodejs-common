import {
  ClientInterface,
  LocaleI18nInterface,
  MessageContextInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ContextLogger } from "../logger";
import { MessageMeta } from "../message";
import { I18n } from "i18n";

export class Context {
  public readonly logger: LoggerService;
  public started: Date;
  private _now: Date;

  constructor(
    public correlationId: string,
    logger: LoggerService,
    public config: DefaultConfig,
    public client: ClientInterface,
    public readonly locale: LocaleI18nInterface,
    public readonly messageContext: MessageContextInterface,
    started?: Date,
    public readonly i18n?: I18n,
    public readonly path?: string
  ) {
    this.logger = new ContextLogger(correlationId, config, client, logger);
    this.started = started || new Date(Date.now());
  }

  public now(): Date {
    if (this._now) {
      return this._now;
    }
    return new Date();
  }

  public setNow(now: Date) {
    this._now = now;
  }

  getMessageMeta(type: string): MessageMetaInterface {
    return new MessageMeta(
      type,
      this.locale,
      this.config,
      this.correlationId,
      this.started
    );
  }
}
