import { CustomParseBoolPipe } from "./custom.parse-bool.pipe";
import { BadRequestException } from "@nestjs/common";

describe(CustomParseBoolPipe.name, () => {
  let pipe: CustomParseBoolPipe;

  beforeEach(() => {
    pipe = new CustomParseBoolPipe();
  });

  it("Should parse 1", () => {
    expect(pipe.transform(1)).toEqual(true);
  });

  it("Should parse 0", () => {
    expect(pipe.transform(0)).toEqual(false);
  });

  it("Should parse true", () => {
    expect(pipe.transform(true)).toEqual(true);
  });

  it("Should parse false", () => {
    expect(pipe.transform(false)).toEqual(false);
  });

  it("Should parse 'true'", () => {
    expect(pipe.transform("true")).toEqual(true);
  });

  it("Should parse 'false'", () => {
    expect(pipe.transform("false")).toEqual(false);
  });

  it("Should parse '1'", () => {
    expect(pipe.transform("1")).toEqual(true);
  });

  it("Should parse '0'", () => {
    expect(pipe.transform("0")).toEqual(false);
  });

  it("Should parse 'TRUE'", () => {
    expect(pipe.transform("TRUE")).toEqual(true);
  });

  it("Should parse 'FALSE'", () => {
    expect(pipe.transform("FALSE")).toEqual(false);
  });

  it("Should not parse 'False'", () => {
    expect(() => {
      pipe.transform("False");
    }).toThrow(BadRequestException);
  });

  it("Should not parse 'True'", () => {
    expect(() => {
      pipe.transform("True");
    }).toThrow(BadRequestException);
  });

  it("Should parse 'yes'", () => {
    expect(pipe.transform("yes")).toEqual(true);
  });

  it("Should parse 'no'", () => {
    expect(pipe.transform("no")).toEqual(false);
  });

  it("Should parse 'YES'", () => {
    expect(pipe.transform("YES")).toEqual(true);
  });

  it("Should parse 'NO'", () => {
    expect(pipe.transform("NO")).toEqual(false);
  });

  it("Should not parse 'No'", () => {
    expect(() => {
      pipe.transform("No");
    }).toThrow(BadRequestException);
  });

  it("Should not parse 'Yes'", () => {
    expect(() => {
      pipe.transform("Yes");
    }).toThrow(BadRequestException);
  });

  it("Should require a value by default", () => {
    expect(() => {
      pipe.transform(undefined);
    }).toThrow(BadRequestException);
  });

  it("Should not require if option specifies to not require it", () => {
    expect(
      new CustomParseBoolPipe({ required: false }).transform(undefined)
    ).toBeUndefined();
  });

  it("Should require if option specifies to require it", () => {
    expect(() => {
      new CustomParseBoolPipe({ required: true }).transform(undefined);
    }).toThrow(BadRequestException);
  });
});
