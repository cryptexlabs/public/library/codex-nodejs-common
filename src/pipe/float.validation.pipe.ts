import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import * as Joi from "joi";

@Injectable()
export class FloatValidationPipe implements PipeTransform {
  private _schema: Joi.Schema;
  constructor() {
    this._schema = Joi.number();
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    if (parseFloat(value).toString() !== value.toString()) {
      throw new BadRequestException(`${value} must be a float`);
    }
    return value;
  }
}
