import {
  BadRequestException,
  Injectable,
  Optional,
  PipeTransform,
} from "@nestjs/common";

@Injectable()
export class CustomParseBoolPipe
  implements PipeTransform<string | undefined | boolean | number, boolean> {
  constructor(@Optional() private readonly options?: { required?: boolean }) {}
  transform(value: string | undefined | boolean | number): boolean | undefined {
    if (["TRUE", "true", "YES", "yes", "1", true, 1].includes(value)) {
      return true;
    } else if (["FALSE", "false", "NO", "no", "0", false, 0].includes(value)) {
      return false;
    } else if (this.options?.required !== false) {
      throw new BadRequestException("Invalid boolean value");
    }
  }
}
