import {
  CodexMetaTypeEnum,
  CountryCode,
  LanguageCode,
} from "@cryptexlabs/codex-data-model";
import { MetricsHostConfig } from "./metrics-host-config";
import { HostConfigInterface } from "./host-config.interface";
import { SwitchConfigInterface } from "./switch-config.interface";
import * as fs from "fs";
import { config, parse } from "dotenv";
import { KafkaConfig } from "./kafka-config";
import { KafkaConfigInterface } from "./kafka-config.interface";
import { JwtConfig } from "./jwt-config";
import { JwtConfigInterface } from "./jwt-config.interface";
import { Injectable } from "@nestjs/common";
import { UrlInterface } from "./url.interface";
import { ElasticsearchConfig } from "./elasticsearch-config";
import { PingConfigInterface } from "./ping-config.interface";
import { ConsumerTypeEnum } from "./consumer-type.enum";
import { JsonSerializableInterface } from "../message";
import { TelemetryConfigInterface } from "./telemetry-config.interface";
import { LOG_LEVEL, DEFAULT_LOG_LEVELS } from "./logging-config.interface";
import { dirname } from "path";
import * as yaml from "js-yaml";
import * as glob from "glob";
import * as watch from "watch";
import * as fastq from "fastq";
import { CustomLogger } from "../logger";
import * as process from "process";
import * as mergeAnything from "@cryptexlabs/merge-anything";
import { RedisConfigInterface } from "./redis-config.interface";
import { SqlConfigInterface } from "./sql-config.interface";
import { PaginationConfigInterface } from "./pagination-config.interface";

@Injectable()
export class DefaultConfig implements JsonSerializableInterface<any> {
  private _appName: string;

  private _serverConfig;
  private _configUpdateQueue;
  private _monitors: string[];
  private _pathsWatching: string[];

  private readonly _basePath: string;
  private readonly _envName: string;
  private _packageVersion: string;
  private readonly _appPath: string;
  private _configChangeLoaderEventHandler: () => Promise<void>;
  private _logger: CustomLogger;

  constructor(
    basePath: string,
    appPath: string,
    private readonly _clientId: string,
    fallbackEnvironment?: string
  ) {
    this._basePath = basePath;
    this._appPath = appPath;
    this._envName = process.env.ENV_NAME || fallbackEnvironment || "";
    this._logger = new CustomLogger("info", this.logLevels);
    this.setup(basePath, appPath, this._envName);

    this._configUpdateQueue = fastq((path, cb) => {
      this._onConfigUpdateQueueMessage(path).then(() => {
        cb(null);
      });
    }, 1);
    this._monitors = [];
    this._pathsWatching = [];
  }

  setConfigChangeLoaderEventHandler(eventHandler: () => Promise<void>): void {
    this._configChangeLoaderEventHandler = eventHandler;
  }

  private async _onConfigUpdateQueueMessage(path) {
    this._serverConfig = this._loadServerConfig(false);
    if (this._configChangeLoaderEventHandler) {
      await this._configChangeLoaderEventHandler();
    }
  }

  public getServerConfig<C = any>(): C {
    if (!this._serverConfig) {
      this._serverConfig = this._loadServerConfig(true);
    }
    return this._serverConfig;
  }

  private _loadServerConfig(startWatchers: boolean): any {
    const serverConfigPath =
      process.env.SERVER_CONFIG_PATH || `${this._appPath}/config/server.yaml`;

    let serverConfig = {} as any;

    if (fs.existsSync(serverConfigPath)) {
      serverConfig = this._loadConfigPath(serverConfigPath);
      if (startWatchers) {
        this._startConfigPatchWatch(serverConfigPath);
      }
    }

    if (process.env.SERVER_CONFIG_PATTERN) {
      serverConfig = this._loadConfigPatterns(serverConfig, startWatchers);
    }

    this._logger.debug(`Server Config`, { serverConfig } as any);

    return serverConfig;
  }

  private _startConfigPatchWatch(path: string) {
    if (this._hotReload) {
      this._logger.debug(`Starting watch for path ${path}`);
      const parent = dirname(path);
      if (!this._monitors.includes(parent)) {
        this._monitors.push(parent);
        this._logger.debug(`Creating monitor for directory ${parent}`);
        watch.createMonitor(dirname(path), {}, (monitor) => {
          monitor.on("changed", (f, curr, prev) => {
            if (typeof f === "string") {
              if (this._pathsWatching.includes(f)) {
                this._logger.debug(
                  `Config changed for path ${f}. Queueing update`
                );
                this._configUpdateQueue.push(f);
              }
            }
          });
        });
      }
      if (!this._pathsWatching.includes(path)) {
        this._pathsWatching.push(path);
      }
    }
  }

  private _loadConfigPath(serverConfigPath: string): any {
    const updatedServerConfig = yaml.load(
      fs.readFileSync(serverConfigPath).toString()
    );
    if (typeof updatedServerConfig !== "object") {
      throw new Error(
        `Invalid config in file ${serverConfigPath} with env variable SERVER_CONFIG_PATH or default path: ${this._appPath}/config/server.yaml`
      );
    }
    return updatedServerConfig as any;
  }

  private _loadConfigPatterns(serverConfig: any, startWatchers: boolean) {
    const paths = glob.sync(process.env.SERVER_CONFIG_PATTERN);
    this._logger.debug(`paths`, { paths } as any);
    let serverConfigUpdated = serverConfig;
    for (const path of paths) {
      if (!path.includes(".yaml")) {
        throw new Error(
          `Invalid path ${path} found for pattern ${process.env.SERVER_CONFIG_PATTERN} with env variable SERVER_CONFIG_PATTERN`
        );
      }

      const c = yaml.load(fs.readFileSync(path).toString()) as any;
      if (typeof c !== "object") {
        throw new Error(`Invalid config in file ${path}`);
      }
      serverConfigUpdated = mergeAnything.merge(c, serverConfigUpdated);

      if (startWatchers) {
        this._startConfigPatchWatch(path);
      }
    }
    return serverConfigUpdated;
  }

  private get _hotReload() {
    return process.env.SERVER_CONFIG_HOT_RELOAD !== "false";
  }

  public toJSON() {
    return {
      fallbackLanguage: this.fallbackLanguage,
      fallbackCountry: this.fallbackCountry,
      appVersion: this.appVersion,
      appName: this.appName,
      clientId: this.clientId,
      apiVersion: this.apiVersion,
      appPrefix: this.appPrefix,
      docsPrefix: this.docsPrefix,
      docsEnabled: this.docsEnabled,
      docsPath: this.docsPath,
      kafka: this.kafka,
      environmentName: this.environmentName,
      logLevels: this.logLevels,
      httpPort: this.httpPort,
      metrics: this.metrics,
      jwt: this.jwt,
      healthzFilePath: this.healthzFilePath,
      elasticsearch: this.elasticsearch,
      consumerType: this.consumerType,
      gpuEnabled: this.gpuEnabled,
      dataDirectory: this.dataDirectory,
      serviceName: this.serviceName,
      logLevel: this.logLevel,
      loggerType: this.loggerType,
      telemetry: this.telemetry,
    };
  }

  protected setup(basePath: string, appPath: string, environment: string) {
    const envBasePath = `${basePath}/env`;
    const configPath = `${envBasePath}/${environment}.env`;
    if (fs.existsSync(configPath)) {
      config({ path: configPath });
    }

    const secretsPath = `${envBasePath}/${environment}${
      environment ? "." : ""
    }secrets.env`;
    if (fs.existsSync(secretsPath)) {
      const envConfig = parse(fs.readFileSync(secretsPath));

      for (const k in envConfig) {
        process.env[k] = envConfig[k];
      }
    }

    const configOverridePath = `${envBasePath}/${environment}.override.env`;
    if (fs.existsSync(configOverridePath)) {
      const envConfig = parse(fs.readFileSync(configOverridePath));

      for (const k in envConfig) {
        process.env[k] = envConfig[k];
      }
    }

    const packageJson = require(`${appPath}/package.json`);
    this._appName = packageJson.name;
    this._packageVersion = packageJson.version;
  }

  public addEnvFile(fileName: string): this {
    const envBasePath = `${this._basePath}/env`;
    const envConfigPath = `${envBasePath}/${this._envName}.${fileName}.env`;
    if (fs.existsSync(envConfigPath)) {
      const envConfig = parse(fs.readFileSync(envConfigPath));

      for (const k in envConfig) {
        process.env[k] = envConfig[k];
      }
    }
    return this;
  }

  public get fallbackLanguage(): LanguageCode {
    return (process.env.LOCALE_LANGUAGE_FALLBACK as LanguageCode) || "en";
  }

  public get fallbackCountry(): CountryCode {
    return (process.env.LOCALE_COUNTRY_FALLBACK as CountryCode) || "US";
  }

  public get appVersion(): string {
    return process.env.APP_VERSION || this._packageVersion;
  }

  public get appName(): string {
    return this._appName;
  }

  public get clientId(): string {
    return this._clientId;
  }

  public get apiVersion(): string {
    return process.env.API_VERSION || "v1";
  }

  public get appPrefix(): string {
    return process.env.APP_PREFIX || "api";
  }

  public get docsPrefix(): string {
    return process.env.DOCS_PREFIX || "docs";
  }

  public get docsEnabled(): boolean {
    return process.env.DOCS_ENABLED === "true";
  }

  public get docsPath(): string {
    return process.env.DOCS_PATH || "";
  }

  public get kafka(): KafkaConfigInterface {
    return new KafkaConfig(
      process.env.KAFKA_ENABLED,
      process.env.KAFKA_HOST,
      process.env.KAFKA_PORT,
      process.env.KAFKA_USERNAME,
      process.env.KAFKA_PASSWORD,
      process.env.SASL_MECHANISM,
      process.env.KAFKA_TOPICS_CONFIG_PATH ||
        `${this._appPath}/config/topics.yaml`,
      process.env.KAFKA_CLIENT_ID || process.env.POD_NAME || this.appName,
      process.env.KAFKA_DEFAULT_PARTITIONS,
      process.env.KAFKA_DEFAULT_ERROR_TOPIC
    );
  }

  get serviceName(): string {
    return process.env.SERVICE_NAME;
  }

  public get environmentName(): string {
    return process.env.ENV_NAME;
  }

  public get logLevels(): string[] {
    return (process.env.LOG_LEVELS || "debug,info,error").trim().split(",");
  }

  get loggerType(): string {
    return process.env.LOGGER_TYPE || "default";
  }

  get logLevel(): LOG_LEVEL {
    return this.logLevels.reduce<LOG_LEVEL>(
      (prev: LOG_LEVEL, curr: LOG_LEVEL) => {
        return DEFAULT_LOG_LEVELS[curr] < DEFAULT_LOG_LEVELS[prev]
          ? curr
          : prev;
      },
      "info"
    );
  }

  public get httpPort(): number {
    return parseInt(process.env.HTTP_PORT || "3000", 10);
  }

  public get externalHttpPort(): number {
    return process.env.EXTERNAL_HTTP_PORT
      ? parseInt(process.env.EXTERNAL_HTTP_PORT, 10)
      : this.httpPort;
  }

  public get metrics(): SwitchConfigInterface & HostConfigInterface {
    return new MetricsHostConfig(
      process.env.GRAPHITE_HOST,
      process.env.GRAPHITE_PORT,
      process.env.METRICS_ENABLED
    );
  }

  get telemetry(): TelemetryConfigInterface {
    return {
      metrics: {
        enabled: process.env.TELEMETRY_METRICS_ENABLED === "true",
        path: process.env.TELEMETRY_METRICS_PATH,
        port: parseInt(process.env.TELEMETRY_METRICS_PORT || "8081", 10),
      },
      traces: {
        enabled: process.env.TELEMETRY_TRACE_ENABLED === "true",
        traceUrl: process.env.TELEMETRY_TRACE_URL || "http://localhost:4317",
      },
    };
  }

  public get jwt(): JwtConfigInterface {
    return new JwtConfig(process.env.JWT_VERIFY, process.env.JWT_AUD);
  }

  public get healthzFilePath(): string {
    return process.env.HEALTHZ_FILE_PATH || "/tmp/healthz";
  }

  public get elasticsearch(): UrlInterface & PingConfigInterface {
    return new ElasticsearchConfig(
      process.env.ELASTICSEARCH_URL || "http://elasticsearch:9200",
      process.env.ELASTICSEARCH_PING_INTERVAL_SECONDS || "10"
    );
  }

  public get consumerType(): ConsumerTypeEnum {
    return (
      (process.env.CONSUMER_TYPE as ConsumerTypeEnum) ||
      ConsumerTypeEnum.DEFAULT
    );
  }

  public get gpuEnabled(): boolean {
    return process.env.GPU_ENABLED === "true";
  }

  public get dataDirectory(): string {
    return process.env.DATA_DIRECTORY || "/data";
  }

  public get overrideOrganization(): string {
    return process.env.OVERRIDE_ORGANIZATION;
  }

  public getMetaType(type: CodexMetaTypeEnum): CodexMetaTypeEnum {
    return (this.overrideOrganization
      ? (type as string).replace(
          /^cryptexlabs\./,
          `${this.overrideOrganization}.`
        )
      : type) as CodexMetaTypeEnum;
  }

  public get apiPrefixes(): string[] {
    return process.env.API_PREFIXES
      ? process.env.API_PREFIXES.split(",")
      : [this.appPrefix];
  }

  public get prettyPrintLogs(): boolean {
    return process.env.PRETTY_PRINT_LOGS !== "false";
  }

  get redis(): RedisConfigInterface {
    return {
      enabled: process.env.REDIS_ENABLED === "true",
      host: process.env.REDIS_HOST ? process.env.REDIS_HOST : "localhost",
      port: parseInt(
        process.env.REDIS_PORT ? process.env.REDIS_PORT : "6379",
        10
      ),
    };
  }

  get pagination(): PaginationConfigInterface {
    return {
      defaultPageSize: parseInt(
        process.env.PAGINATION_DEFAULT_PAGESIZE || "20",
        10
      ),
      maxPageSize: parseInt(process.env.PAGINATION_MAX_PAGESIZE || "200", 10),
    };
  }

  get sql(): SqlConfigInterface {
    return {
      database: process.env.SQL_DATABASE,
      client: process.env.SQL_CLIENT,
      userName: process.env.SQL_USERNAME,
      password: process.env.SQL_PASSWORD,
      primary: {
        hostName: process.env.SQL_HOSTNAME,
        port: parseInt(process.env.SQL_PORT, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_MIN, 10),
          max: parseInt(process.env.SQL_POOL_MAX, 10),
        },
      },
      reader: {
        hostName: process.env.SQL_HOSTNAME_RO,
        port: parseInt(process.env.SQL_PORT_RO, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_RO_MIN, 10),
          max: parseInt(process.env.SQL_POOL_RO_MAX, 10),
        },
      },
    };
  }

  public get authfUrl(): string {
    return process.env.AUTHF_URL || "http://localhost:3007";
  }

  public get authEnabled(): boolean {
    return (
      process.env.AUTH_ENABLED === "true" ||
      !["localhost", "docker"].includes(this.environmentName)
    );
  }

  public get blacklistRefreshIntervalInSeconds(): number {
    return process.env.BLACKLIST_REFRESH_INTERVAL_IN_SECONDS
      ? parseInt(process.env.BLACKLIST_REFRESH_INTERVAL_IN_SECONDS, 10)
      : 15;
  }
}
