import { HostConfigInterface } from "./host-config.interface";

export class HostConfig implements HostConfigInterface {
  public readonly port: number;

  constructor(public readonly host, port: string) {
    this.port = parseInt(port, 10);
  }
}
