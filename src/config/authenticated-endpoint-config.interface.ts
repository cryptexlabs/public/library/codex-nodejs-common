import { EndpointConfigInterface } from "./endpoint-config.interface";

export interface AuthenticatedEndpointConfigInterface
  extends EndpointConfigInterface {
  username: string;
  password: string;
}
