export interface PaginationConfigInterface {
  defaultPageSize: number;
  maxPageSize: number;
}
