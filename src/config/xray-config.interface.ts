export interface XrayConfigInterface {
  enabled: boolean;
  serviceName: string;
}
