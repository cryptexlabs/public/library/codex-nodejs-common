import { AuthenticatedEndpointConfigInterface } from "./authenticated-endpoint-config.interface";
import { EndpointConfig } from "./endpoint-config";

export class AuthenticatedEndpointConfig
  extends EndpointConfig
  implements AuthenticatedEndpointConfigInterface {
  constructor(
    host: string,
    port: string,
    public readonly username: string,
    public readonly password: string
  ) {
    super(host, port);
  }

  get endpoint() {
    return `${this.host}:${this.port}`;
  }
}
