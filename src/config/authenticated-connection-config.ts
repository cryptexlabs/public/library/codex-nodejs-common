import { AuthenticatedConnectionConfigInterface } from "./authenticated-connection-config.interface";
import { ConnectionConfig } from "./connection-config";

export class AuthenticatedConnectionConfig
  extends ConnectionConfig
  implements AuthenticatedConnectionConfigInterface {
  username: string;
  password: string;

  get connectionUrl() {
    return `${this.protocol}://${this.username}:${this.password}@${this.endpoint}`;
  }
}
