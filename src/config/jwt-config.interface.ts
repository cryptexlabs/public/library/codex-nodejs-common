export interface JwtConfigInterface {
  verify: boolean;
  aud: string;
}
