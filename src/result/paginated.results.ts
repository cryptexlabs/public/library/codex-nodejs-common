export interface PaginatedResults<T> {
  total: number;
  results: T[];
}
