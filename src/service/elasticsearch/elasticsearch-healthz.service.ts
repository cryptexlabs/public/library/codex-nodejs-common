import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../../config";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { HealthzComponentEnum, HealthzService } from "../healthz";
import { ElasticsearchPinger } from "./elasticsearch.pinger";

@Injectable()
export class ElasticsearchHealthzService {
  private isWaitingForHealthy: boolean = false;

  private isStarted: boolean = false;
  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    @Inject("ELASTICSEARCH_SERVICE")
    private readonly elasticsearchService: ElasticsearchService,
    @Inject("ELASTICSEARCH_PINGER")
    private readonly elasticsearchPinger: ElasticsearchPinger,
    @Inject("HEALTHZ_SERVICE") private readonly healthzService: HealthzService
  ) {}

  async start() {
    if (!this.isStarted) {
      this.isStarted = true;
      await this.healthzService.makeUnhealthy(
        HealthzComponentEnum.DATABASE_ELASTICSEARCH
      );
      this._ping().then();
      setInterval(() => {
        this._ping();
      }, this.config.elasticsearch.pingIntervalSeconds * 1000);
    }
  }

  async waitForHealthy(): Promise<void> {
    if (!this.isWaitingForHealthy) {
      this.isWaitingForHealthy = true;
      this.logger.log("Waiting for elasticsearch connection", "info");

      await this.elasticsearchPinger.waitForReady();

      this.logger.log("Elasticsearch service connected", "info");

      await this.healthzService.makeHealthy(
        HealthzComponentEnum.DATABASE_ELASTICSEARCH
      );
      this.isWaitingForHealthy = false;
    } else {
      await this.elasticsearchPinger.waitForReady();
    }
  }

  private async _ping() {
    const success = await this.elasticsearchService.ping();
    if (success) {
      this.logger.verbose("Elasticsearch service was successfully pinged");
      await this.healthzService.makeHealthy(
        HealthzComponentEnum.DATABASE_ELASTICSEARCH
      );
    } else {
      this.logger.error("Elasticsearch service could not be reached", "");
      await this.healthzService.makeUnhealthy(
        HealthzComponentEnum.DATABASE_ELASTICSEARCH
      );
    }
  }
}
