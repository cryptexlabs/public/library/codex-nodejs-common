import { Inject, Injectable } from "@nestjs/common";
import { CustomLogger } from "../../logger";

@Injectable()
export class ElasticsearchPinger {
  private intervalId;

  private ready;

  private isPinging;

  constructor(
    @Inject("ELASTICSEARCH_SERVICE") private readonly elasticsearchService,
    @Inject("LOGGER") private readonly logger: CustomLogger
  ) {
    this.intervalId = null;
    this._resetPinger();
  }

  private _resetPinger() {
    if (this.intervalId) {
      this.logger.debug("ES Pinger already running");
      return;
    }
    this.ready = false;
    this.logger.debug("Starting new ES pinger");
    this.intervalId = setInterval(async () => {
      this.logger.info("Waiting for elasticsearch");
      this.ready = await this._isReady();
      if (this.ready) {
        clearInterval(this.intervalId);
        this.intervalId = null;
      } else {
        this.logger.debug("Elasticsearch not available", "debug");
      }
    }, 1000);
  }

  public async waitForReady() {
    if (this.ready) {
      this._resetPinger();
    }

    return new Promise((resolve) => {
      const intervalId = setInterval(() => {
        if (this.ready) {
          clearInterval(intervalId);
          resolve(undefined);
        }
      }, 500);
    });
  }
  async _isReady(): Promise<boolean> {
    return new Promise(async (resolve) => {
      try {
        const result = await this.elasticsearchService.ping();
        resolve(result);
      } catch (e) {
        this.logger.error(
          e.message || "Elasticsearch cannot be reached",
          e.trace || ""
        );
        resolve(false);
      }
    });
  }
}
