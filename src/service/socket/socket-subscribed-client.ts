import { HttpStatus, UnauthorizedException } from "@nestjs/common";
import { TopicAuthorizorInterface } from "../../auth";

export class SocketSubscribedClient {
  private topics: (string | RegExp)[];

  constructor(
    private readonly token: string,
    private readonly client: WebSocket,
    private readonly authorizors: TopicAuthorizorInterface[]
  ) {
    this.topics = [];
  }

  public async subscribeToTopics(topics: string[]): Promise<void> {
    for (const topic of topics) {
      if (!(await this._isAuthorized(topic))) {
        this.client.close(1000, `${HttpStatus.FORBIDDEN}: Forbidden`);
        throw new UnauthorizedException(
          new Error(`Not allowed to subscribe to topic '${topic}'`)
        );
      }
    }
    this.topics = topics;
  }

  public getTopics(): (string | RegExp)[] {
    return this.topics;
  }

  private async _isAuthorized(topic: string): Promise<boolean> {
    for (const authorizor of this.authorizors) {
      if (await authorizor.authorize(topic, this.token)) {
        return true;
      }
    }
    return false;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public forwardFromTopic(topic: string, message: any): void {
    if (this.topics.includes(topic)) {
      this.client.send(message);
    }
  }
}
