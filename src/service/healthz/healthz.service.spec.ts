import { Logger } from "@nestjs/common";
import { HealthzComponentEnum } from "./healthz.component.enum";
import { HealthzService } from "./healthz.service";
import { DefaultConfig } from "../../config";
import { instance, mock, when } from "ts-mockito";
import { unlink, writeFile } from "fs";
import { mocked } from "jest-mock";

jest.mock("fs", () => ({
  unlink: jest.fn().mockImplementation((file, cb) => cb()),
  writeFile: jest
    .fn()
    .mockImplementation((file, content: ArrayBufferView, cb) => cb()),
  stat: jest
    .fn()
    .mockImplementation((file, cb) =>
      cb(file === "/tmp/healthz" ? true : undefined, {})
    ),
  existsSync: jest.fn().mockImplementation((file) => file !== "/tmp/healthz"),
}));

jest.mock("glob", () => ({
  sync: jest.fn(),
}));

describe("HealthzService", () => {
  let service: HealthzService;
  let config;
  let MockedConfig: DefaultConfig;
  let LoggerMock: Logger;

  beforeEach(() => {
    LoggerMock = mock(Logger);
    MockedConfig = mock(DefaultConfig);
    config = instance(MockedConfig);
    when(MockedConfig.healthzFilePath).thenReturn("/tmp/healthz");
    service = new HealthzService(config, instance(LoggerMock));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.resetModules();
  });

  it("Should be healthy when no components were made healthy or unhealthy", async () => {
    expect(await service.isHealthy()).toBe(true);
  });

  it("Should be healthy when all components are healthy", async () => {
    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    expect(await service.isHealthy()).toBe(true);
  });

  it("Should be unhealthy when at least 1 components are unhealthy", async () => {
    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    await service.makeUnhealthy(HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
    expect(await service.isHealthy()).toBe(false);
  });

  it("Should delete healthz file when made unhealthy and file exists", async () => {
    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    const mockedUnlink = await mocked(unlink, true);
    when(MockedConfig.healthzFilePath).thenReturn("/tmp/noexist");
    expect(mockedUnlink.mock.calls.length).toBe(0);
    await service.makeUnhealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    expect(mockedUnlink.mock.calls.length).toBe(1);
  });

  it("Should delete healthz file when made unhealthy and file does not exist", async () => {
    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    const mockedUnlink = await mocked(unlink, true);
    expect(mockedUnlink.mock.calls.length).toBe(0);
    await service.makeUnhealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    expect(mockedUnlink.mock.calls.length).toBe(0);
  });

  it("Should create healthz file when all components are made healthy", async () => {
    const mockedWriteFile = await mocked(writeFile, true);

    // Make both components unhealthy
    await service.makeUnhealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);
    await service.makeUnhealthy(HealthzComponentEnum.MESSAGE_BROKER_KAFKA);

    // Make 1 components healthy
    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);

    expect(mockedWriteFile.mock.calls.length).toBe(0);

    // Make other component healthy
    await service.makeHealthy(HealthzComponentEnum.MESSAGE_BROKER_KAFKA);

    expect(mockedWriteFile.mock.calls.length).toBe(1);
  });

  it("Should not create healthz file when file already is created", async () => {
    const mockedWriteFile = await mocked(writeFile, true);

    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);

    expect(mockedWriteFile.mock.calls.length).toBe(1);

    when(MockedConfig.healthzFilePath).thenReturn("/tmp/noexist");

    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);

    expect(mockedWriteFile.mock.calls.length).toBe(1);

    await service.makeHealthy(HealthzComponentEnum.DATABASE_ELASTICSEARCH);

    expect(mockedWriteFile.mock.calls.length).toBe(1);
  });
});
