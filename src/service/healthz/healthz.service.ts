import { Inject, Injectable, LoggerService } from "@nestjs/common";
import * as fs from "fs";
import { DefaultConfig } from "../../config";
import { HealthzComponentEnum } from "./healthz.component.enum";

@Injectable()
export class HealthzService {
  private readonly componentHealth: Record<
    HealthzComponentEnum | string,
    boolean
  >;

  private _isHealthy: boolean;

  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService
  ) {
    this.componentHealth = {};
    this._isHealthy = false;
  }

  async isHealthy(): Promise<boolean> {
    if (Object.keys(this.componentHealth).length === 0) {
      return true;
    }

    return this._allComponentsAreHealthy();
  }

  async makeHealthy(component: HealthzComponentEnum | string): Promise<void> {
    this.componentHealth[component] = true;

    if (!this._allComponentsAreHealthy()) {
      return Promise.resolve();
    }

    return new Promise(async (resolve) => {
      if (fs.existsSync(this.config.healthzFilePath)) {
        resolve();
      } else {
        if (!this._isHealthy) {
          this._isHealthy = true;
          this.logger.debug("Making service healthy", "info");
        }
        fs.writeFile(this.config.healthzFilePath, "I'm healthy!", () => {
          resolve();
        });
      }
    });
  }

  async makeUnhealthy(component: HealthzComponentEnum): Promise<void> {
    this.componentHealth[component] = false;

    if (fs.existsSync(this.config.healthzFilePath)) {
      return new Promise((resolve) => {
        if (this._isHealthy) {
          this._isHealthy = false;
          this.logger.debug("Making service unhealthy");
        }
        fs.unlink(this.config.healthzFilePath, () => {
          resolve();
        });
      });
    }
  }

  private _allComponentsAreHealthy(): boolean {
    for (const componentName in this.componentHealth) {
      const healthy = this.componentHealth[componentName];
      if (!healthy) {
        this.logger.debug(`${componentName} is still unhealthy`);
        return false;
      }
    }
    return true;
  }
}
