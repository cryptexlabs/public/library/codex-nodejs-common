import { Injectable, LoggerService } from "@nestjs/common";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@cryptexlabs/codex-data-model";
import { Message } from "kafkajs";

@Injectable()
export class KafkaStubService implements KafkaServiceInterface {
  async connect(): Promise<void> {}

  async disconnect(): Promise<void> {}

  async initializeConsumer(consumerGroup: string, config?: {}): Promise<void> {}

  async initializeProducer(): Promise<void> {}

  async publish(topic: string, payload: any): Promise<void> {}

  async publishBulk(topic: string, payloads: any[]): Promise<void> {}

  async send(
    payload: MessageInterface<any>,
    logger: LoggerService
  ): Promise<any> {}

  async startConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>,
    err?: (error: string) => Promise<void>
  ): Promise<void> {}

  async startManualConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>
  ): Promise<void> {}

  async startProducer(): Promise<any> {}

  async stopConsumer(): Promise<void> {}

  async ensureTopicsExist(
    topics: (string | RegExp)[],
    waitForLeaders?: boolean
  ): Promise<void> {}
}
