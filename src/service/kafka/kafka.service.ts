import { Inject, Injectable, LoggerService } from "@nestjs/common";
import {
  Admin,
  Consumer,
  ConsumerConfig,
  EachMessagePayload,
  ITopicConfig,
  Kafka,
  Message,
  Producer,
  SASLMechanism,
} from "kafkajs";
import { DefaultConfig } from "../../config";
import { ConsumerServiceDelegateInterface } from "../consumer";
import { KafkaLogger } from "./kafka.logger";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@cryptexlabs/codex-data-model";

@Injectable()
export class KafkaService
  implements KafkaServiceInterface, ConsumerServiceDelegateInterface {
  private _kafka: Kafka;
  private _kafkaConsumer: Consumer;
  private _kafkaProducer: Producer;
  private _kafkaAdmin: Admin;

  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    extraConfig: any = {}
  ) {
    const kafkaClientConfig = {
      clientId: config.clientId,
      brokers: config.kafka.endpoint.split(","),
      sasl: undefined,
      ssl: undefined,
      ...extraConfig,
    };

    if (config.kafka.username) {
      kafkaClientConfig.sasl = {
        mechanism: config.kafka.saslMechanism.toLowerCase() as SASLMechanism,
        username: config.kafka.username,
        password: config.kafka.password,
      };
      kafkaClientConfig.ssl = true;
    }

    const kafkaLogger = new KafkaLogger(logger);

    this._kafka = new Kafka({
      ...kafkaClientConfig,
      logCreator: (logLevel) => {
        return kafkaLogger.log.bind(kafkaLogger);
      },
    });
  }

  async stopConsumer(): Promise<void> {
    await this._kafkaConsumer.stop();
  }

  public async connect(): Promise<void> {
    if (this._kafkaConsumer) {
      await this._kafkaConsumer.connect();
    }
    if (this._kafkaProducer) {
      await this._kafkaProducer.connect();
    }
    this._kafkaAdmin = this._kafka.admin();
    await this._kafkaAdmin.connect();
  }

  public async disconnect(): Promise<void> {
    if (this._kafkaConsumer) {
      await this._kafkaConsumer.disconnect();
    }
    if (this._kafkaProducer) {
      await this._kafkaProducer.disconnect();
    }
  }

  public async initializeConsumer(
    consumerGroup: string,
    config: ConsumerConfig = {} as ConsumerConfig
  ): Promise<void> {
    this._kafkaConsumer = this._kafka.consumer({
      groupId: consumerGroup,
      ...config,
    });
  }

  public async initializeProducer(): Promise<void> {
    this._kafkaProducer = this._kafka.producer();
  }

  public async startConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>,
    err?: (error: string) => Promise<void>
  ): Promise<void> {
    const promises = [];
    for (const topic of topics) {
      promises.push(
        this._kafkaConsumer.subscribe({ topic, fromBeginning: false })
      );
    }
    try {
      await Promise.all(promises);
    } catch (e) {
      this.logger.error(e.message, e.trace);
    }

    await this.ensureTopicsExist(topics);
    if (this.config.kafka.defaultKafkaErrorTopic) {
      await this.ensureTopicsExist([this.config.kafka.defaultKafkaErrorTopic]);
    }
    await this._kafkaConsumer.run({
      autoCommit: true,
      eachMessage: async ({ topic, partition, message }) => {
        this.logger.log({
          topic,
          partition,
          message: {
            key: message.key,
            value: message.value.toString(),
          },
        });
        await callback(topic, message);
      },
    });

    this._kafkaConsumer.on(this._kafkaConsumer.events.CRASH, async (event) => {
      const error = event?.payload?.error;
      this.logger.log("Consumer is crashing!");
      if (error) {
        this.logger.error(JSON.stringify(error));
      }
      await err("Consumer crashed");
    });
  }

  public async startManualConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>
  ): Promise<void> {
    await this._setManualConsume();

    const promises = [];
    for (const topic of topics) {
      promises.push(
        this._kafkaConsumer.subscribe({ topic, fromBeginning: true })
      );
    }
    try {
      await Promise.all(promises);

      await this.ensureTopicsExist(topics);

      const results = [
        this._kafkaConsumer.run({
          eachMessage: async (messagePayload: EachMessagePayload) => {
            const { topic, partition, message } = messagePayload;
            await callback(topic, message);
          },
        }),
      ];
      await Promise.all(results);
    } catch (e) {
      this.logger.error(e.message, e.trace);
    }
  }

  public async ensureTopicsExist(
    topics: (string | RegExp)[],
    waitForLeaders: boolean = true
  ): Promise<void> {
    const stringTopics = [];
    for (const topic of topics) {
      if (typeof topic === "string") {
        stringTopics.push(topic);
      }
    }

    const currentTopics = await this._kafkaAdmin.listTopics();
    const createTopics: ITopicConfig[] = [];
    for (const topic of stringTopics) {
      if (!currentTopics.includes(topic)) {
        const createTopic: ITopicConfig = {
          numPartitions: this.config.kafka.defaultPartitions,
          topic,
        };
        createTopics.push(createTopic);
      }
    }

    if (createTopics.length > 0) {
      await this._kafkaAdmin.createTopics({
        waitForLeaders,
        topics: createTopics,
      });
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async publish(
    topic: string,
    payload: MessageInterface<any>
  ): Promise<void> {
    const json = JSON.stringify(payload);
    this.logger.debug(`Publishing kafka message: ${json} to topic ${topic}`);
    await this._kafkaProducer.send({
      topic,
      messages: [{ value: json }],
    });
  }

  public async publishBulk(topic: string, payloads: any[]): Promise<void> {
    const messages = [];

    for (const payload of payloads) {
      messages.push({ value: JSON.stringify(payload), key: topic });
    }

    await this._kafkaProducer.send({
      topic,
      messages,
    });
  }

  private async _setManualConsume(): Promise<void> {
    /* Based on solution provided here:
     * https://github.com/tulios/kafkajs/issues/825#issuecomment-674106799
     *
     * 1. We need to know which partitions we are assigned.
     * 2. Which partitions have we consumed the last offset for
     * 3. If all partitions have 0 lag, we exit.
     */

    /*
     * `consumedTopicPartitions` will be an object of all topic-partitions
     * and a boolean indicating whether or not we have consumed all
     * messages in that topic-partition. For example:
     *
     * {
     *   "topic-test-0": false,
     *   "topic-test-1": false,
     *   "topic-test-2": false
     * }
     */
    let consumedTopicPartitions = {};
    this._kafkaConsumer.on(
      this._kafkaConsumer.events.GROUP_JOIN,
      async ({ payload }) => {
        const { memberAssignment } = payload;

        consumedTopicPartitions = Object.entries(memberAssignment).reduce(
          (topics, [topic, partitions]) => {
            for (const partition in partitions) {
              this.logger.debug("topic-partition: ", `${topic}-${partition}`);
              topics[`${topic}-${partition}`] = false;
            }
            return topics;
          },
          {}
        );
      }
    );

    /*
     * This is extremely unergonomic, but if we are currently caught up to the head
     * of all topic-partitions, we won't actually get any batches, which means we'll
     * never find out that we are actually caught up. So as a workaround, what we can do
     * is to check in `FETCH_START` if we have previously made a fetch without
     * processing any batches in between. If so, it means that we received empty
     * fetch responses, meaning there was no more data to fetch.
     *
     * We need to initially set this to true, or we would immediately exit.
     */
    let processedBatch = true;
    this._kafkaConsumer.on(this._kafkaConsumer.events.FETCH_START, async () => {
      if (processedBatch === false) {
        await this._kafkaConsumer.disconnect();
        process.exit(0);
      }

      processedBatch = false;
    });

    /*
     * Now whenever we have finished processing a batch, we'll update `consumedTopicPartitions`
     * and exit if all topic-partitions have been consumed,
     */
    this._kafkaConsumer.on(
      this._kafkaConsumer.events.END_BATCH_PROCESS,
      async ({ payload }) => {
        const { topic, partition, offsetLag } = payload;

        consumedTopicPartitions[`${topic}-${partition}`] = offsetLag === "0";

        if (
          Object.values(consumedTopicPartitions).every((consumed) =>
            Boolean(consumed)
          )
        ) {
          await this._kafkaConsumer.disconnect();
          process.exit(0);
        }

        processedBatch = true;
      }
    );

    // Graceful shutdown
    const errorTypes = ["unhandledRejection", "uncaughtException"];
    const signalTraps: NodeJS.Signals[] = ["SIGTERM", "SIGINT", "SIGUSR2"];

    errorTypes.map((type) => {
      process.on(type, async (e) => {
        try {
          this.logger.debug(`process.on ${type}`);
          this.logger.error(e);
          await this.disconnect();
          process.exit(0);
        } catch (_) {
          process.exit(1);
        }
      });
    });

    signalTraps.map((type) => {
      process.once(type, async () => {
        try {
          await this.disconnect();
        } finally {
          process.kill(process.pid, type);
        }
      });
    });
  }
}
