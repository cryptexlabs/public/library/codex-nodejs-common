import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { KafkaService } from "./kafka.service";
import { DefaultConfig } from "../../config";

@Injectable()
export class ReplayKafkaMessages {
  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    private readonly kafkaService: KafkaService
  ) {}

  async process(
    groupId: string,
    inTopic: string,
    outTopic: string
  ): Promise<void> {
    await this.kafkaService.initializeProducer();
    await this.kafkaService.initializeConsumer(groupId);
    await this.kafkaService.connect();

    await this.kafkaService.startManualConsumer(
      [inTopic],
      async (_, message) => {
        await this.kafkaService.publish(
          outTopic,
          JSON.parse(message.value.toString())
        );
      }
    );
    await this.kafkaService.disconnect();
  }
}
