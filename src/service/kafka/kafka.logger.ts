import { LoggerService } from "@nestjs/common";
import { logLevel } from "kafkajs";

export class KafkaLogger {
  constructor(private readonly logger: LoggerService) {}

  private toContext(level: logLevel): string {
    switch (level) {
      case logLevel.ERROR:
      case logLevel.NOTHING:
        return "error";
      case logLevel.WARN:
        return "warn";
      case logLevel.DEBUG:
        return "debug";
      default:
        return "info";
    }
  }

  log({ level, log }) {
    const logJson = JSON.stringify(log);
    const logObject = JSON.parse(logJson);
    let finalLevel = level;
    if (level === logLevel.ERROR) {
      if (
        logObject.error === "The group is rebalancing, so a rejoin is needed"
      ) {
        finalLevel = logLevel.INFO;
      }
    }
    this.logger.log(logJson, this.toContext(finalLevel));
  }
}
