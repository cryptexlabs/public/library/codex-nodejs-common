import { Message } from "kafkajs";
import { MessageInterface } from "@cryptexlabs/codex-data-model";

export interface KafkaServiceInterface {
  stopConsumer(): Promise<void>;

  connect(): Promise<void>;

  disconnect(): Promise<void>;

  initializeConsumer(consumerGroup: string, config?): Promise<void>;

  initializeProducer(): Promise<void>;

  startConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>,
    err?: (error: string) => Promise<void>
  ): Promise<void>;

  startManualConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>
  ): Promise<void>;

  publish(topic: string, payload: MessageInterface<any>): Promise<void>;

  publishBulk(topic: string, payloads: any[]): Promise<void>;

  ensureTopicsExist(
    topics: (string | RegExp)[],
    waitForLeaders?: boolean
  ): Promise<void>;
}
