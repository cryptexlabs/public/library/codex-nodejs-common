import { StringUtil } from "../../util";

export class ConsumerUtil {
  private constructor() {}

  static getTopics(topics: string[]): (RegExp | string)[] {
    const newTopics: (RegExp | string)[] = [];
    for (const topic of topics) {
      const trimTopic = topic.trim();
      if (StringUtil.isRegexString(trimTopic)) {
        const regexString = trimTopic
          .substr(0, 1)
          .substr(trimTopic.length - 2, 1);
        newTopics.push(new RegExp(regexString));
      } else {
        newTopics.push(topic.trim());
      }
    }
    return newTopics;
  }
}
