import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { Message } from "kafkajs";
import { ConsumerServiceDelegateInterface } from "./consumer-service-delegate.interface";
import { ConsumerTypeEnum, DefaultConfig } from "../../config";
import { EventHandler, EventHandlerFactoryInterface } from "../../event";
import * as jsYaml from "js-yaml";
import * as fs from "fs";
import { TopicsConfigInterface } from "../../config/topics-config.interface";
import { StringUtil } from "../../util";
import { ConsumerUtil } from "./consumer.util";
import { HealthzComponentEnum, HealthzService } from "../healthz";

// eslint-disable-next-line @typescript-eslint/no-var-requires
// tslint:disable-next-line:no-var-requires
const randomstring = require("randomstring");

@Injectable()
export class ConsumerService {
  private topics: TopicsConfigInterface;
  private eventHandler: EventHandler;

  constructor(
    @Inject("HEALTHZ_SERVICE")
    private readonly healthzService: HealthzService,
    @Inject("CONSUMER_SERVICE_DELEGATE")
    private readonly consumerDelegateService: ConsumerServiceDelegateInterface,
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    @Inject("EXTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")
    private readonly externalFactory: EventHandlerFactoryInterface,
    @Inject("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")
    private readonly internalFactory: EventHandlerFactoryInterface,
    @Inject("CONSUMER_TYPE") private readonly consumerType: string
  ) {
    this.topics = jsYaml.safeLoad(
      fs.readFileSync(config.kafka.topicsConfigPath).toString()
    ) as TopicsConfigInterface;
    this.eventHandler = new EventHandler(logger);
    logger.debug(`Initializing consumer with type: ${consumerType}`);
  }

  async start(consumerConfig = {}): Promise<void> {
    await this.healthzService.makeUnhealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );
    if (this.config.kafka.defaultKafkaErrorTopic) {
      await this.consumerDelegateService.initializeProducer();
    }
    await this.consumerDelegateService.initializeConsumer(
      `${this.config.appName}-${this.consumerType}`,
      consumerConfig
    );
    await this.consumerDelegateService.connect();

    const topics = await this._getTopics();
    await this.consumerDelegateService.startConsumer(
      topics,
      async (topic: string, message: Message) => {
        await this.handle(topic, message);
      },
      async (error: string) => {
        this.logger.error(error);
        await this.healthzService.makeUnhealthy(
          HealthzComponentEnum.MESSAGE_BROKER_KAFKA
        );
      }
    );
    await this.healthzService.makeHealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );
  }

  private _getInternalTopics() {
    const internalTopicTypeMap = {
      [ConsumerTypeEnum.DEFAULT]: "internal",
      [ConsumerTypeEnum.GPU]: "gpu",
    };
    return this.topics.topics[internalTopicTypeMap[this.consumerType]];
  }

  private async _getTopics() {
    let allTopics = [];
    if (this.consumerType === ConsumerTypeEnum.DEFAULT) {
      allTopics = allTopics.concat(this.topics.topics.external);
      allTopics = allTopics.concat(this.topics.topics.internal);
      this.logger.debug(
        `Subscribing to external topics: ${JSON.stringify(
          this.topics.topics.external
        )}`
      );
      this.logger.debug(
        `Subscribing to internal topics: ${JSON.stringify(
          this.topics.topics.internal
        )}`
      );
    }
    if (this.consumerType === ConsumerTypeEnum.GPU) {
      allTopics = allTopics.concat(this.topics.topics.gpu);
      this.logger.debug(
        `Subscribing to gpu topics: ${JSON.stringify(this.topics.topics.gpu)}`
      );
    }

    return ConsumerUtil.getTopics(allTopics);
  }

  private async _errorEventHandler(message: any, errorMessage: string) {
    const messageWithError = { ...message };
    messageWithError.meta.errorMessage = errorMessage;
    await this.consumerDelegateService.publish(
      this.config.kafka.defaultKafkaErrorTopic,
      {
        ...messageWithError,
      }
    );
  }

  public async handle(topic: string, message: any): Promise<void> {
    const data = JSON.parse(message.value.toString());
    const messageTopic = data.meta?.type ?? topic;
    const handler = this._getFactory(messageTopic).getHandler(messageTopic);
    let errorEventHandler;
    if (this.config.kafka.defaultKafkaErrorTopic)
      errorEventHandler = this._errorEventHandler.bind(this);
    await this.eventHandler.handle(
      message,
      messageTopic,
      handler,
      errorEventHandler
    );
  }

  public _getFactory(topic: string): EventHandlerFactoryInterface {
    for (const testTopic of this.topics.topics.external) {
      if (StringUtil.stringMatches(testTopic, topic)) {
        return this.externalFactory;
      }
    }
    for (const testTopic of this._getInternalTopics()) {
      if (StringUtil.stringMatches(testTopic, topic)) {
        return this.internalFactory;
      }
    }
    throw new Error(`Could not match any factory for topic: ${topic}`);
  }
}
