export enum DataServiceOperationEnum {
  CREATED = "created",
  UPDATED = "updated",
  NOOP = "noop", // Not updated or created
}
