import { I18n } from "i18n";
import * as packageJSON from "../../package.json";
import { en_US } from "./index";

const i18nData = {} as i18nAPI;

const i18nInstance = new I18n();

i18nInstance.configure({
  locales: packageJSON.i18n.languages,
  defaultLocale: "en-US",
  register: i18nData,
  updateFiles: false,
  objectNotation: true,
  autoReload: false,
  syncFiles: false,
  extension: ".json",
  staticCatalog: {
    "en-US": en_US,
  },
});

export { i18nData, i18nInstance as i18n };
